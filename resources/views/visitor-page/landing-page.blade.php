@extends('visitor-layout.main')

@section('content')
<section id="jumbo">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 center-verhor">
                <div class="bag">
                    <h2>Spices Signature</h2>
                    <h5>Original Nusantara Cuisine</h5>
                </div>
            </div>
            <div class="col-lg-6 center-verhor">
                <img src="assets/img//gallery/plate.png" alt="">
            </div>
        </div>
    </div>
</section>

<section id="promo">
    <div class="container">
        <h1 class="header-section">Promo</h1>

        <div class="row">
            @foreach ($promo as $promo)
            <div class="col-lg-4">
                <a href="">
                    <div class="card">
                        <img src="{{ asset('assets/admin/promo/'.$promo->img) }}" alt="">
                        <div class="bag">
                            <h4 class="">{{ $promo->nama_promo }}</h4>
                            <h5>{!! $promo->deskripsi !!}</h5>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
</section>

@if ($data_isExist == '')
<section id="about">
    <div class="container">
        <h1 class="header-section">About Spice Signature</h1>
    </div>
</section>
@else
<section id="about">
    <div class="container">
        <h1 class="header-section">About Spice Signature</h1>

        <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="{{ asset('assets/admin/about/'.$about->img_1) }}" class="d-block w-100" alt="img" style="width: 100%; height: 450px; object-fit: cover;">
                </div>
                <div class="carousel-item">
                    <img src="{{ asset('assets/admin/about/'.$about->img_2) }}" class="d-block w-100" alt="img" style="width: 100%; height: 450px; object-fit: cover;">
                </div>
                <div class="carousel-item">
                    <img src="{{ asset('assets/admin/about/'.$about->img_3) }}" class="d-block w-100" alt="img" style="width: 100%; height: 450px; object-fit: cover;">
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>

        <h5>{{ $about->judul }}</h5>
        <p>{!! $about->deskripsi !!}</p>
    </div>
</section>
@endif

<section id="gallery">
    <div class="container">
        <h1 class="header-section">Gallery</h1>

        <div class="row">
            @foreach ($data as $gallery)
            <div class="col-6 col-lg-2">
                <div class="card">
                    <img src="{{ asset('assets/img/gallery/'.$gallery->img) }}" onclick="openModal();currentSlide(1)" class="hover-shadow cursor">
                </div>
            </div>
            @endforeach
        </div>

        <div id="myModal" class="modal" style="z-index: 1031">
            <span class="close cursor" onclick="closeModal()">&times;</span>
            <div class="modal-content">

                @foreach ($data as $gallery)

                <div class="mySlides">
                    <div class="numbertext">x / XX</div>
                    <img src="{{ asset('assets/img/gallery/'.$gallery->img) }}">
                </div>

                <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                <a class="next" onclick="plusSlides(1)">&#10095;</a>

                @endforeach

            </div>
        </div>

    </div>
</section>

<section id="menu">
    <div class="container" id="all-product">
        <h1 class="header-section">Menu</h1>

        <div uk-filter="target: .js-filter">

            <ul class="uk-subnav uk-subnav-pill d-flex justify-content-center">
                <li class="uk-active" uk-filter-control=".all"><a href="#">Semua</a></li>
                @foreach ($kategori as $j)
                <li uk-filter-control=".{{ $j->kategori }}"><a href="#">{{ $j->kategori }}</a></li>
                {{-- <li uk-filter-control=".combo"><a href="#">Combo</a></li> --}}
                @endforeach
            </ul>

            <div class="row js-filter uk-child-width-1-2 uk-child-width-1-3@m uk-text-center">
                @foreach ($menu as $i)
                    <div class="col-lg-3 {{$i->kategori->kategori}} all">
                        <div class="card">
                            <img src="{{ asset('assets/admin/menu/'.$i->img) }}" alt="{{$i->kategori->kategori}}">
                            <div class="bag">
                                <h4 class="">{{$i->nama_menu}}</h4>
                                <h5>@currency ($i->harga)</h5>
                                <button type="button" class="btn btn-primary-ss" data-bs-toggle="modal" data-bs-target="#addToCart{{ $i->id }}">
                                    <i class="fas fa-cart-plus"></i> Add To Cart
                                </button>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </div>
</section>

    @foreach ($data as $i)
    <div class="modal fade modalAddToCart" id="addToCart{{ $i->id }}" tabindex="-1" aria-labelledby="addToCartLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <form action="{{ route('cart.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body text-center">
                        <img src="{{ asset('assets/admin/menu/'.$i->img) }}" alt="" style="width: 100%">
                        <input type="hidden" value="{{ $i->img }}"  name="image">
                        <div>
                            <input type="hidden" value="{{ $i->id }}"  name="id">
                            <input type="hidden" value="{{ $i->nama_menu }}"  name="name">
                            <input type="hidden" value="{{ $i->harga }}"  name="price">
                            {{ $i->nama_menu }} - @currency($i->harga)
                        </div>
                        @if (Auth::user())
                        <div>
                            Orders : <input name="quantity" type="number" value="1" min="1" max="10">
                        </div>
                        <div>
                            <button class="btn {{ Request::is('*panel/cart*') ? 'btn-secondary' : 'btn-secondary-ss' }} btn-sm" data-bs-dismiss="modal" aria-label="Close">Cancel</button>
                            <button class="btn {{ Request::is('*panel/cart*') ? 'btn-primary' : 'btn-primary-ss' }} btn-sm">Add To Cart</button>
                        </div>
                        @else
                        <div>
                            Login terlebih dahulu untuk melakukan order <br> <br>
                            <a class="btn btn-secondary-ss" href="{{ route('login') }}">Login</a>
                        </div>
                        @endif

                    </div>
                </form>
            </div>
        </div>
    </div>
    @endforeach

@endsection
