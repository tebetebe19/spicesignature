@extends('kitchen-layout.main')

@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Dashboards</h1>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-4">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="text-center">Jumlah menu yang terjual</h3>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped" id="tableOrder">
                                <thead class="text-center success">
                                    <tr>
                                        <th>No.</th>
                                        <th>Menu</th>
                                        <th>Jumlah</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    @foreach ($jualByDay as $key => $i)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $i->nama_menu }}</td>
                                        <td>{{ $i->qty }} Porsi</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-8">
                    <div id="calendar"></div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection

@section('js')
<script type="text/javascript">
    $(document).ready(function() {

        // pass _token in all ajax
         $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // initialize calendar in all events
        var calendar = $('#calendar').fullCalendar({
            editable: true,
            header:{
                left:'prev,next today',
                center:'title',
                right:'month,agendaWeek,agendaDay'
            },
            events: "{{ route('order-kitchen.index') }}",
            displayEventTime: true,
            eventRender: function (event, element, view) {
                if (event.allDay === 'true') {
                        event.allDay = true;
                } else {
                        event.allDay = false;
                }
            },
        });
    });
</script>
@endsection
