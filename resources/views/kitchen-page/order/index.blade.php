@extends('kitchen-layout.main')

@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Order</h1>
        </div>
        <div class="section-body">
            <h2 class="section-title">Ready to Cook</h2>
            <div class="row">
                @foreach ($antrian as $i)
                    @if ($i->finish == NULL)
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="card card-danger">
                            <div class="card-header" >
                                <h4 style="color: red">{{ $i->transaksi->user->name }} - {{ $i->no_resi }}</h4>
                                <div class="card-header-action">
                                    <div class="btn btn-danger no_resi" data-id="{{ $i->no_resi }}" >
                                        Antrian : {{ $i->no_antrian }}
                                    </div>
                                </div>
                            </div>
                            <div id="data-order">
                                <div class="card-body">
                                    @foreach ($i->transaksi->detail_transaksi as $j)
                                    <div style="font-weight: bolder">
                                        {{ $j->menu->kategori->kategori }}
                                    </div>
                                    <ul>
                                        <li>[{{ $j->qty }}] x {{ $j->menu->nama_menu }}</li>
                                    </ul>
                                    @endforeach
                                    <div class="text-right">
                                        <u>{{ $i->created_at->isoformat('dddd, d-MMMM-Y') }}</u><br>
                                        <i class="fas fa-clock"></i> <i>{{ $i->start->diffForHumans() }}</i>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button class="btn btn-danger" data-toggle="modal" data-target="#cariKurir{{ $i->no_resi }}" style="width: 100%">Order Ready, Pesan Kurir</button>
                            </div>
                        </div>
                    </div>
                    @else
                    {{-- <div class="col-12 col-md-6 col-lg-4">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h4>Selesai</h4>
                            </div>
                            <div class="card-body">
                                <p>{{ $i->transaksi->user->name }} - {{ $i->no_resi }}</p>
                                <p>Selesai : <i class="fas fa-clock"></i> <i>{{ $i->finish->diffForHumans() }}</i></p>
                            </div>
                        </div>
                    </div> --}}
                    @endif
                @endforeach
            </div>



            <h2 class="section-title">On The Way</h2>
            <div class="row">
                @foreach ($kurir as $i)
                @if ($i->status == "Sedang dalam Perjalanan")
                {{-- Loop Card Sedang Diantar start --}}
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card card-info">
                        <div class="card-header">
                            <h4 style="color: #3ABAF4">{{ $i->transaksi->user->name }} - {{ $i->no_resi }}</h4>
                            <div class="card-header-action">
                                <div class="btn btn-info">
                                    No. Antrian : {{ $i->antrian_order->no_antrian }}
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            {{-- Looping Kategori start --}}
                                @foreach ($i->transaksi->detail_transaksi as $j)
                                <div style="font-weight: bolder">
                                    {{ $j->menu->kategori->kategori }}
                                    <ul>
                                        <li>[{{ $j->qty }}] x {{ $j->menu->nama_menu }}</li>
                                    </ul>
                                </div>
                                @endforeach
                            <span style="color: green; font-weight: bolder">GOJEK</span> '{{ $i->name }}' - {{ $i->no_plat }} <a href="{{ $i->link_track }}" target="blank_" class="btn btn-secondary ml-2 my-3">Track Kurir</a>
                            <div class="text-right">
                                <u>{{ $i->updated_at->isoformat('dddd, d-MMMM-Y') }}</u><br>
                                <i class="fas fa-clock"></i> <i>{{ $i->updated_at->diffForHumans() }}</i>
                            </div>
                            {{-- Looping Kategori end --}}
                        </div>
                        <div class="card-footer text-right">
                            <button class="btn btn-info" data-toggle="modal" data-target="#selesaiKurir{{ $i->no_resi }}" style="width: 100%">Kurir Sudah Sampai</button>
                        </div>
                    </div>
                </div>
                @endif
                {{-- Loop Card Sedang Diantar end --}}
                @endforeach
            </div>

            <h2 class="section-title">Delivered</h2>
            <div class="row">
                @foreach ($kurir as $i)
                @if ($i->status == "Kurir sudah sampai")
                {{-- Loop Card Sedang Diantar start --}}
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="card card-success">
                        <div class="card-header">
                            <h4>{{ $i->transaksi->user->name }} - {{ $i->no_resi }}</h4>
                            <div class="card-header-action">
                                <div class="btn btn-success">
                                    No. Antrian : {{ $i->antrian_order->no_antrian }}
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            {{-- Looping Kategori start --}}
                                @foreach ($i->transaksi->detail_transaksi as $j)
                                <div style="font-weight: bolder">
                                    {{ $j->menu->kategori->kategori }}
                                    <ul>
                                        <li>[{{ $j->qty }}] x {{ $j->menu->nama_menu }}</li>
                                    </ul>
                                </div>
                                @endforeach
                            <span style="color: green; font-weight: bolder">GOJEK</span> '{{ $i->name }}' - {{ $i->no_plat }} <a href="{{ $i->link_track }}" class="btn btn-secondary ml-2 my-3">Track Kurir</a>
                            <div class="text-right">
                                <u>{{ $i->updated_at->isoformat('dddd, d-MMMM-Y') }}</u><br>
                                <i class="fas fa-clock"></i> <i>{{ $i->updated_at->diffForHumans() }}</i>
                            </div>
                            {{-- Looping Kategori end --}}
                        </div>
                    </div>
                </div>
                @endif
                {{-- Loop Card Sedang Diantar end --}}
                @endforeach
            </div>
        </div>
    </section>
</div>

@include('kitchen-page.order.modal')
@endsection

@section('js')
{{-- <script type="text/javascript">
    $(document).ready(function(){
        var idx = $('.no_resi').attr('data-id')
        // console.log(idx);
        var data_order = $('#data-order')
        data_order.html('')

        $.ajax({
            url:"{{ url('kitchen/order') }}/"+idx,
            method:'get',
            success:function(res){
                // console.log(res)
                $.each(res.detail_transaksi, function(idk, val){
                    data_order.append(`
                        <div class="card-body">
                            <div style="font-weight: bolder">
                                ${val.menu.kategori.kategori}
                            </div>
                            <ul>
                                <li>[${val.qty}] x [${val.menu.nama_menu}]</li>
                            </ul>
                        </div>
                    `)
                })
            },
            error: function(xhr, status, error) {
                // alert(xhr.responseText);
                alert("error")
            }
        })
    })
</script> --}}
@endsection
