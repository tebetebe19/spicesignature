@foreach ($antrian as $i)
<!-- Modal Info Kurir -->
<form action="{{ url('kitchen/kurir', $i->no_resi) }}" method="post" enctype="multipart/form-data">
    {{-- <form action="{{ route('kurir.store') }}" method="post" enctype="multipart/form-data"> --}}
    @csrf
    @method('PATCH')
    <div class="modal fade" id="cariKurir{{ $i->no_resi }}" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    Informasi Kurir
                </div>
                <div class="modal-body">
                    <input type="text" class="form-control" name="no_resi" value="{{ $i->transaksi->no_resi }}">
                    <div class="form-group">
                        <label>Nama Kurir</label>
                        <input type="text" name="name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Plat Nomor</label>
                        <input type="text" name="no_plat" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Link Track Kurir</label>
                        <input type="url" name="link_track" class="form-control">
                    </div>
                </div>
                <div class="modal-footer text-right">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endforeach


@foreach ($kurir as $i)
<form action="{{ url('kitchen/status_kurir', $i->no_resi) }}" method="post" enctype="multipart/form-data">
    @csrf
    @method('PATCH')
    <div class="modal fade" id="selesaiKurir{{ $i->no_resi }}" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    Informasi Kurir
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Nama Kurir</label>
                        <input type="text" class="form-control" name="name" value="{{ $i->name }}">
                    </div>
                    <div class="form-group">
                        <label>Plat Nomor</label>
                        <input type="text" class="form-control" name="no_plat" value="{{ $i->no_plat }}">
                    </div>
                    <div class="form-group">
                        <label>Link Track Kurir</label>
                        <input type="text" class="form-control" name="link_track" value="{{ $i->link_track }}">
                    </div>
                    <div class="form-group">
                        <label>No. Resi</label>
                        <input type="text" class="form-control" name="no_resi" value="{{ $i->no_resi }}" readonly>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="status_kurir" value="Kurir sudah sampai" readonly>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">Close</span>
                    </button>
                    <button type="submit" class="btn btn-primary">
                        <span aria-hidden="true">Kurir Sudah Sampai</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>
@endforeach
