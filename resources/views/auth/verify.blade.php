@extends('visitor-layout.main')

@section('content')
<section id="login">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1 class="header-section">{{ __('Verifikasi Email') }}</h1>
                <div class="row">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Link Verifikasi sudah dikirim ke email anda, silahkan verifikasi untuk melanjutkan.') }}
                        </div>
                    @endif
                    <h4>
                        {{ __('Link Verifikasi sudah dikirim ke email anda, silahkan verifikasi untuk melanjutkan.') }}
                    </h4>

                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-lg btn-primary">{{ __('Belum menerima Email ?') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
