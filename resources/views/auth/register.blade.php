@extends('visitor-layout.main')

@section('content')
<section id="login">
    <form action="{{ route('register') }}" method="POST">
        @csrf
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-4">
                    <div class="row mb-3">
                        <label for="name" class="form-label">Name</label>
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="row mb-3">
                        <label for="email" class="form-label">Email</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        
                        <small id="emailHelp" class="form-text text-muted">Active Email</small>
                    </div>

                    <div class="row mb-3">
                        <label for="password" class="form-label">Password</label>

                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="row mb-3">
                        <label for="password-confirm" class="form-label">Confirm Password</label>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                    </div>
                    <div class="row mb-3">
                        {!! htmlFormSnippet() !!}
                    </div>
                    <button type="submit" class="btn btn-secondary-ss">
                        {{ __('Register') }}
                    </button>
                    <br><br>
                    <h5 class="text-center">
                        Already have account?  <a class="btn btn-primary-ss" href="/login">Login here</a> 
                    </h5>
                </div>

            </div>
        </div>
    </form>
</section>
@endsection
