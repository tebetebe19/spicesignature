@extends('visitor-layout.main')

@section('content')
<section id="login">
    <form method="POST" action="{{ route('login') }}">
        @csrf
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-4">
                    <div class="mb-3">
                        <label for="email" class="form-label">Email</label>
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email" aria-describedby="emailHelp">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label for="password" class="form-label">Password</label>
                        <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="password">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <div class="col-md-6"> {!! htmlFormSnippet() !!} </div>
                    </div>
                    <div class="mb-3 form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-check-label" for="remember">
                            {{ __('Remember Me') }}
                        </label>
                    </div>
                    <button type="submit" class="btn btn-secondary-ss" style="width: 100%">
                        {{ __('Login') }}
                    </button> 
                    <br><br>
                    <h5 class="text-center">
                        Dont have account?  <a class="btn btn-primary-ss" href="/register">Create here</a> 
                    </h5>
                </div>
            </div>
        </div>
    </form>
</section>
@endsection
