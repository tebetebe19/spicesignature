<section id="navbar">
    <nav class="navbar navbar-light navbar-expand-md bg-faded justify-content-center text-center">
        <div class="container">
            <a href="/" class="navbar-brand d-flex w-50 me-auto">
                <img src="assets/img/logo/nav-logo.png" alt="" style="width: 90px;">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#collapsingNavbar3">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="navbar-collapse collapse w-100" id="collapsingNavbar3">
                <ul class="navbar-nav w-100 justify-content-center">
                    <li class="nav-item active">
                        <a class="nav-link" href="#promo">Promo</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#about">About Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#gallery">Gallery</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#menu">Menu</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav ms-auto w-100 justify-content-end">
                    <li class="nav-item">


                        @if(Auth::user())
                            @if (Auth::user()->role_id == 3)
                                <div class="d-sm-none d-lg-inline-block">
                                    <a href="{{ route('setting-profile.index') }}" class="btn btn-secondary-ss">
                                        Hi, {{ Auth::user()->name }}
                                    </a>
                                </div>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                                @elseif(Auth::user()->role_id == 2)
                                <div class="d-sm-none d-lg-inline-block">
                                    <a href="{{ route('dashboard-kitchen.index') }}" class="btn btn-secondary-ss">
                                        Hi, {{ Auth::user()->name }}
                                    </a>
                                </div>
                                @elseif(Auth::user()->role_id == 1)
                                <div class="d-sm-none d-lg-inline-block">
                                    <a href="{{ route('order-admin.index') }}" class="btn btn-secondary-ss">
                                        Hi, {{ Auth::user()->name }}
                                    </a>
                                </div>
                            @endif
                        @else
                        <a href="/register" class="btn btn-primary-ss" style="background-color: transparent">
                            Register
                        </a>
                        <a href="/login" class="btn btn-secondary-ss">
                            Log In
                        </a>
                        @endif

                    </li>
                </ul>
            </div>
        </div>
    </nav>
</section>
