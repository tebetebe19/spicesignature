<button class="btn btn-secondary-ss" style="position: fixed; bottom:30px; right: 30px; border-radius: 30px" data-bs-toggle="modal" data-bs-target="#modalContactUs">
    <i class="fas fa-headset"></i> Contact Us
</button>

<div class="modal modal-contact fade" id="modalContactUs" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-body text-center">
        <div class="bag mb-2">
            <div class="row">
            <div class="col-12">
                <i class="fab fa-whatsapp"></i> <br>
                <button class="btn btn-secondary-ss">081312341234</button>
            </div>
            <div class="col-12">
                <i class="fab fa-instagram"></i> <br>
                <button class="btn btn-secondary-ss">@spicesignature</button>
            </div>
            <div class="col-12">
                <i class="far fa-envelope"></i> <br>
                <button class="btn btn-secondary-ss">info@spicesignature.com</button>
            </div>
            </div>
        </div>
        <button type="button" class="btn btn-primary-ss" data-bs-dismiss="modal" aria-label="Close">Close</button>
        </div>
    </div>
    </div>
</div>