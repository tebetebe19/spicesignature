<section id="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="section-title">
                    Spice Signature
                </div>
                <div>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellendus id nulla laborum illum. Illo exercitationem dolores mollitia minus atque ipsum placeat doloribus unde optio nam rerum ratione praesentium quasi, iusto ducimus eveniet at adipisci, expedita nesciunt? Asperiores eum maiores tempore ad quasi harum, beatae ipsum nihil dignissimos adipisci neque odio.
                </div>
            </div>
            <div class="col-lg-4 text-center">
                <div class="section-title">
                    Navigation
                </div>
                <ul>
                    <li>
                        <a href="#promo">Promo</a>
                    </li>
                    <li>
                        <a href="#about">About Us</a>
                    </li>
                    <li>
                        <a href="#gallery">Gallery</a>
                    </li>
                    <li>
                        <a href="#menu">Menu</a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-4">
                <div class="section-title">
                    Contact Us
                </div>
                <div>
                    Gandaria 8 Office Tower, 19th floor Unit B, Jalan Sultan Iskandar Muda No.10 Jakarta 12240 <br> <br>
                    (icon) @instagram <br>
                    (icon) (+62)813 1234-2134 <br>
                    (icon) info@spicesignature.com
                </div>
            </div>
        </div>
    </div>
</section>

<section id="copyright">
    2022 &copy; Spice Signature
</section>
