{{-- Modal Tambah --}}
<form action="{{ route('promo.store') }}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Tambah Menu</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Thumbnail</label>
                        <div class="file-upload">
                            <img src="" width="40%" id="photo" class="foto_promo">
                            <input class="form-control photo" id="foto" accept="image/*" onchange="readURL(this,$(&quot;.foto_promo&quot;))" name="img" type="file">
                        </div>
                        <small id="passwordHelpBlock" class="form-text text-muted">
                            Requirement foto 500x500 px
                        </small>
                    </div>

                    <div class="form-group">
                        <label>Nama Promo</label>
                        <input type="text" class="form-control" placeholder="Nama Promo" name="nama_promo" required>
                    </div>

                    <div class="form-group">
                        <label>Deskripsi</label>
                        <textarea name="deskripsi" id="deskripsi" cols="30" rows="10" class="form-control"></textarea>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </div>
        </div>
    </div>
</form>
{{-- End of Modal Tambah --}}


{{-- Modal Edit --}}
@foreach ($data as $i)
    <div class="modal fade" id="edit{{ $i->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <form action="{{ url('admin/promo', $i->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Edit Menu</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Thumbnail</label>
                            <div class="file-upload">
                                <img src="{{asset('assets/admin/promo/'.$i->img)}}" width="40%" id="photo" class="foto_promo">
                                <input class="form-control photo" id="foto" accept="image/*" onchange="readURL(this,$(&quot;.foto_promo&quot;))" name="img" type="file">
                                <small id="emailHelp" class="form-text text-muted">Requirement foto 500x500 px</small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Nama Promo</label>
                            <input type="text" name="nama_promo" class="form-control" value="{{ $i->nama_promo }}">
                        </div>

                        <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea name="deskripsi" id="deskripsi" cols="30" rows="10" class="form-control">{!! $i->deskripsi !!}</textarea>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endforeach
{{-- End of Modal Edit --}}

{{-- Modal Hapus --}}
@foreach ($data as $i)
    <div class="modal fade" id="hapus{{ $i->id }}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <form action="{{ url('admin/promo',$i->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('DELETE')
            <div class="modal-dialog modal-dialog-centered modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="mySmallModalLabel">Hapus Menu</h4>
                    </div>
                    <div class="modal-body text-center">
                        Apa anda yakin ingin menghapus data <br>
                         <b>{{$i->nama_promo}}</b> ?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Ya</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endforeach
{{-- End of Modal Hapus --}}
