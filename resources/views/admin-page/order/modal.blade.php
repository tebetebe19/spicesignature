<!-- Modal Detail Menu -->
<div class="modal fade" id="detailMenu" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body" style="background-color: var(--primary)">
                <div class="row">
                    <div class="col-12" >
                        <div class="card">
                            <div class="card-body" style="text-center">
                                <h2 style="text-align:center; margin-bottom:0px; color:var(--primary)">Detail Order</h2>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <div id="penerima" style="font-weight: bolder; font-size: larger; color: var(--primary)">
                                </div>
                                <div>
                                    <i class="fas fa-map-marker-alt mr-2" style="color: red"></i> <span id="alamat"></span>
                                </div>
                                <div>
                                    <i class="fas fa-mobile-alt mr-2" style="color: rgb(91, 91, 231)" ></i> <span id="phone"></span><a href=""><i class="fas fa-pencil-alt ml-4"></i></a>
                                </div>
                            </div>
                        </div>
                        <div id="data-order" class="card card-cart-preview">
                            {{-- Ditampilkan menggunakan JSON --}}
                        </div>

                        <div class="card">
                            <div class="card-body text-right">
                                <p>Ongkos Kirim : <span id="ongkir"></span></p>
                                <p>Total : <span id="total"></span></p>
                                <p>Grand Total : <span id="grand_total"></span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@foreach($detail_order as $i)
<!-- Modal Lihat Bukti TF -->
<div class="modal fade" id="lihatTF{{ $i->no_resi }}" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action="">
                <div class="modal-header">

                </div>
                <div class="modal-body">
                    <img src="{{ asset('assets/admin/bukti_transfer/'.$i->order->bukti_transfer) }}" alt="" style="width: 100%">
                </div>
                <div class="card-footer text-right">
                    <button class="btn btn-secondary mr-1" type="submit">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endforeach

@foreach($detail_order as $i)
<form action="{{ url('admin/order-admin', $i->order->id) }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PATCH')
    <!-- Modal Terima Pembayaran -->
    <div class="modal fade" id="terimaTF{{ $i->no_resi }}" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                    <div class="modal-header">

                    </div>
                    <div class="modal-body text-center">
                        <img src="{{ asset('assets/admin/bukti_transfer/'.$i->order->bukti_transfer) }}" alt="" style="width: 100%">
                        <input type="hidden" name="transaksi_id" value="{{ $i->id }}">
                        <input type="hidden" name="no_resi" value="{{ $i->no_resi }}">
                        <button class="btn btn-warning mt-4" type="submit">Terima Pembayaran</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endforeach

<!-- Modal Info Kurir -->
<div class="modal fade modalAddToCart" id="infoKurir" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                Informasi Kurir
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Nama Kurir</label>
                    <input type="text" class="form-control" disabled>
                </div>
                <div class="form-group">
                    <label>Plat Nomor</label>
                    <input type="text" class="form-control" disabled>
                </div>
                <a href="#" class="btn btn-success" style="width: 100%">Track Kurir</a>
            </div>
            <div class="card-footer text-right">
                <button type="button" class="btn btn-primary" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">Close</span>
                </button>
            </div>
        </div>
    </div>
</div>

