@extends('admin-layout.main')

@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Order List</h1>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="tableOrder">
                                    <thead>
                                    <tr class="text-center">
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th>No.Invoice</th>
                                        <th>Order Date</th>
                                        <th>Total Transaksi</th>
                                        <th>Ongkos Kirim</th>
                                        <th>Menu</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($detail_order as $key => $i)
                                        <tr class="text-center">
                                            <td class="align-middle">
                                                {{ $key+1 }}
                                            </td>
                                            <td class="align-middle">
                                                {{ $i->user->name }}
                                            </td>
                                            <td class="align-middle">
                                                {{ $i->no_resi }}
                                            </td>
                                            <td class="align-middle">
                                                {{ $i->created_at }}
                                            </td>
                                            <td class="align-middle">
                                                @currency($i->total)
                                            </td>
                                            <td class="align-middle">
                                                @currency($i->ongkir)
                                            </td>
                                            <td class="align-middle">
                                                <button class="btn btn-info" id="btnDetailMenu" data-id="{{ $i->no_resi }}">Detail Menu</button>
                                            </td>
                                            @if ($i->order->status_order == "Pembayaran diterima")
                                            <td class="align-middle" style="color: green; font-weight: bolder">
                                                <span class="badge badge-sm badge-success">{{ $i->order->status_order }}</span>
                                            </td>
                                            @elseif ($i->order->status_order == "Pembayaran sedang direview")
                                            <td class="align-middle" style="color: red; font-weight: bolder">
                                                <span class="badge badge-sm badge-warning">{{ $i->order->status_order }}</span>
                                            </td>
                                            @elseif ($i->order->status_order == "Menunggu Pembayaran")
                                            <td class="align-middle" style="color: red; font-weight: bolder">
                                                <span class="badge badge-sm badge-warning">{{ $i->order->status_order }}</span>
                                            </td>
                                            @else
                                            <td class="align-middle" style="color: red; font-weight: bolder">
                                                <span class="badge badge-sm badge-info">{{ $i->order->status_order }}</span>
                                            </td>
                                            @endif
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr class="text-center">
                                            <td colspan="4">TOTAL</td>
                                            <td class="align-middle">@currency($total_transaksi)</td>
                                            <td class="align-middle">@currency($total_ongkir)</td>
                                            <td colspan="2"></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@include('admin-page.order.modal')

@endsection

@section('js')
<script type="text/javascript">
    $("#tableOrder").dataTable({
        "columnDefs": [
            { "sortable": false, "targets": [7] }
        ]
    });
    $(document).on('click', '#btnDetailMenu', function() {
        $('#detailMenu').modal('show');
        var idx = $(this).attr('data-id')
        // console.log(idx);
        var data_order = $('#data-order')
        data_order.html('')
        $.ajax({
            url:"{{ url('admin/order-admin') }}/"+idx,
            method:'get',
            success:function(res){
                $('#penerima').text(res.user.name)
                $('#phone').text(res.user.nohp)
                $('#alamat').text(res.shipping.detail_alamat)
                $('#ongkir').text(res.ongkir)
                $('#total').text(res.total)
                $('#grand_total').text(res.grand_total)
                // console.log(res)
                $.each(res.detail_transaksi, function(idk,val){
                    sub_total = parseInt(val.menu.harga * val.qty)
                    data_order.append(`
                        <div class="card-body">
                                <img src="{{ asset('assets/admin/menu/${val.menu.img}') }}" alt="" style="">
                                <div class="bag">
                                    <div>
                                        <div class="summary mb-2">
                                            ${val.qty} x ${val.menu.nama_menu }
                                        </div>
                                        <div class="total">
                                            Subtotal :  ${currency(sub_total)}
                                        </div>
                                    </div>
                                </div>
                            </div>
                    `)

                })
            },
            error: function(xhr, status, error) {
                // alert(xhr.responseText);
                alert("error")
            }
        })
    })
    function currency(val){
        bilangan = val;
        var number_string = bilangan.toString(),
            sisa    = number_string.length % 3,
            rupiah  = number_string.substr(0, sisa),
            ribuan  = number_string.substr(sisa).match(/\d{3}/g);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        return "Rp. "+rupiah
    }
</script>
@endsection
