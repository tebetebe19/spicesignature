{{-- Modal Tambah --}}
<form action="{{ route('role.store') }}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Tambah Data</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Role</label>
                        <input type="text" class="form-control" placeholder="Role User" name="name" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </div>
        </div>
    </div>
</form>
{{-- End of Modal Tambah --}}


{{-- Modal Edit --}}
@foreach ($data as $i)
    <div class="modal fade" id="roleEdit{{ $i->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <form action="{{ url('admin/role', $i->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Edit Role</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Role</label>
                            <input type="text" name="name" class="form-control" value="{{ $i->name }}">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endforeach
{{-- End of Modal Edit --}}

{{-- Modal Hapus --}}
@foreach ($data as $i)
    <div class="modal fade" id="roleHapus{{ $i->id }}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <form action="{{ url('admin/role',$i->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('DELETE')
            <div class="modal-dialog modal-dialog-centered modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="mySmallModalLabel">Hapus Data</h4>
                    </div>
                    <div class="modal-body text-center">
                        Apa anda yakin ingin menghapus data <br>
                         <b>{{$i->name}}</b> ?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Ya</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endforeach
{{-- End of Modal Hapus --}}
