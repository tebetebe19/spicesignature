{{-- Modal Tambah --}}
<form action="{{ route('kategori.store') }}" method="post" enctype="multipart/form-data" name="kategori">
    @csrf
    <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Tambah Data</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Nama Kategori</label>
                        <input type="text" class="form-control" name="kategori">
                        <small class="form-text text-muted">Jangan melebihi 1 kata</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </div>
        </div>
    </div>
</form>
{{-- End of Modal Tambah --}}


{{-- Modal Edit --}}
@foreach ($data as $i)
    <div class="modal fade" id="kategoriEdit{{ $i->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <form action="{{ url('admin/kategori', $i->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Edit Kategori</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Nama Kategori</label>
                            <input type="text" name="kategori" class="form-control" value="{{ $i->kategori }}">
                            <small class="form-text text-muted">Jangan melebihi 1 kata</small>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endforeach
{{-- End of Modal Edit --}}

{{-- Modal Hapus --}}
@foreach ($data as $i)
    <div class="modal fade" id="kategoriHapus{{ $i->id }}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <form action="{{ url('admin/kategori',$i->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('DELETE')
            <div class="modal-dialog modal-dialog-centered modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="mySmallModalLabel">Hapus Data</h4>
                    </div>
                    <div class="modal-body text-center">
                        Apa anda yakin ingin menghapus data <br>
                         <b>{{$i->kategori}}</b> ?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Ya</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endforeach
{{-- End of Modal Hapus --}}
