@extends('admin-layout.main')

@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>List Kategori</h1>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                    <div class="card-header">
                        <button type="button" class="btn btn-primary m-b-sm" data-toggle="modal" data-target="#tambah"><i class="fa fa-plus"></i> Tambah Data</button>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-kategori" id="tableKategori">
                                <thead>
                                    <tr class="text-center">
                                        <th>#</th>
                                        <th>Kategori</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    {{-- @foreach ($data as $key => $kat)
                                    <tr class="text-center">
                                        <td>
                                            {{ $key+1 }}
                                        </td>
                                        <td class="align-middle">
                                            {{ $kat->kategori }}
                                        </td>
                                        <td class="align-middle">
                                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#kategoriEdit{{$kat->id}}" title="Edit"><i class="fa fa-pencil"></i> Edit</button>
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#kategoriHapus{{$kat->id}}" title="Hapus"><i class="fa fa-trash"></i> Hapus</button>
                                        </td>
                                    </tr>
                                    @endforeach --}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@include('admin-page.kategori.crud')

@endsection

@section('js')
    <script type="text/javascript">
        $(function(){
            $("form[name='kategori']").validate({
                rules: {
                    kategori:{
                        required: true,
                        minlength: 3,
                        maxlength: 20
                    }
                },
                messages: {
                    kategori: {
                        required: "Kategori tidak boleh kosong !",
                        minlength: "Kategori tidak boleh kurang dari 4 Karakter !",
                        maxlength: "Kategori tidak boleh lebih dari 20 Karakter !"
                    }
                }
            })
        })

        $(function(){
            var table = $('.table-kategori').DataTable({
                processing:true,
                serverSide:true,
                ajax: "{{ route('kategori.index') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'kategori', name: 'kategori'},
                    {
                        data: 'action',
                        name: 'action',
                        orderable: true,
                        searchable: true
                    },
                ]
            })
        })

        // $("#tableMenu").dataTable({
        //     "columnDefs": [
        //         { "sortable": false, "targets": [1,5] }
        //     ]
        // });
    </script>

    <script>
        function readURL(input, photo) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('.image-upload-wrap').hide();
                    photo.attr('src', e.target.result);
                    photo.attr('href', e.target.result);
                    $('.file-upload-content').show();
                    $('.image-title').html(input.files[0].name);
                };
                reader.readAsDataURL(input.files[0]);
            } else {
                removeUpload();
            }
        }

        function removeUpload() {
            $('.file-upload-input').replaceWith($('.file-upload-input').clone());
            $('.file-upload-content').hide();
            $('.image-upload-wrap').show();
        }
        $('.image-upload-wrap').bind('dragover', function () {
            $('.image-upload-wrap').addClass('image-dropping');
        });
        $('.image-upload-wrap').bind('dragleave', function () {
            $('.image-upload-wrap').removeClass('image-dropping');
        });

    </script>

    @endsection
