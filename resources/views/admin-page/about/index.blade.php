@extends('admin-layout.main')

@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>About Us</h1>
        </div>
        <div class="section-body">
            <div class="card">
                <div class="card-header">
                    @if ($data_isExist)
                    <a href="#"></a>
                    @else
                    <a class="btn btn-primary" href="{{route('about.create')}}">Tambah Data</a>
                    @endif
                </div>
                <div class="card-body">
                    <table class="table table-striped" id="tableMenu">
                        <thead>
                            <tr>
                                <th>Judul</th>
                                <th>Deskripsi</th>
                                <th>Image 1</th>
                                <th>Image 2</th>
                                <th>Image 3</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($about as $about)
                            <tr>
                                <td>{{ $about->judul }}</td>
                                <td>{{ Str::limit($about->deskripsi, 50) }}</td>
                                <td>
                                    <img src="{{asset('assets/admin/about/'.$about->img_1)}}" alt="" style="width: 150px">
                                </td>
                                <td>
                                    <img src="{{asset('assets/admin/about/'.$about->img_2)}}" alt="" style="width: 150px">
                                </td>
                                <td>
                                    <img src="{{asset('assets/admin/about/'.$about->img_3)}}" alt="" style="width: 150px">
                                </td>
                                <td>
                                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#edit{{$about->id}}" title="Edit"><i class="fa fa-pencil"></i> Edit</button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>

@foreach ($data as $i)
<div class="modal fade" id="edit{{ $i->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <form action="{{ url('admin/about', $i->id) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PATCH')
        <div class="modal-dialog modal-xl modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Edit Menu</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Judul</label>
                        <input type="text" name="judul" class="form-control" value="{{ $i->judul }}">
                    </div>
                    <div class="form-group">
                        <label for="deskripsi">Deskripsi</label>
                        <textarea name="deskripsi" id="deskripsi"class="form-control">{{ $i->deskripsi }}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Image 1</label>
                        <div class="file-upload">
                            <img src="{{ asset('assets/admin/about/'.$i->img_1) }}" width="30%" id="photo" class="foto_promo1">
                            <input class="form-control photo" id="foto" accept="image/*" onchange="readURL(this,$(&quot;.foto_promo1&quot;))" name="img_1" type="file">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Image 2</label>
                        <div class="file-upload">
                            <img src="{{ asset('assets/admin/about/'.$i->img_2) }}" width="30%" id="photo" class="foto_promo2">
                            <input class="form-control photo" id="foto" accept="image/*" onchange="readURL(this,$(&quot;.foto_promo2&quot;))" name="img_2" type="file">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Image 3</label>
                        <div class="file-upload">
                            <img src="{{ asset('assets/admin/about/'.$i->img_3) }}" width="30%" id="photo" class="foto_promo3">
                            <input class="form-control photo" id="foto" accept="image/*" onchange="readURL(this,$(&quot;.foto_promo3&quot;))" name="img_3" type="file">
                        </div>
                    </div>

                    {{-- <div class="form-group">
                        <label>Thumbnail</label>
                        <div class="file-upload">
                            <img src="{{asset('assets/admin/menu/'.$i->img)}}" width="40%" id="photo" class="foto_promo">
                            <input class="form-control photo" id="foto" accept="image/*" onchange="readURL(this,$(&quot;.foto_promo&quot;))" name="img" type="file">
                            <small id="emailHelp" class="form-text text-muted">Requirement foto 500x500 px</small>

                        </div>
                    </div> --}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </div>
        </div>
    </form>
</div>

@endforeach

@endsection

@section('js')
<script>

    function readURL(input, photo) {
    if (input.files && input.files[0]) {

        var reader = new FileReader();

        reader.onload = function(e) {
        $('.image-upload-wrap').hide();
        photo.attr('src', e.target.result);
        photo.attr('href', e.target.result);
        $('.file-upload-content').show();
        $('.image-title').html(input.files[0].name);
        // $('.file-upload-image').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);

        } else {
            removeUpload();
        }
    }

    function removeUpload() {
        $('.file-upload-input').replaceWith($('.file-upload-input').clone());
        $('.file-upload-content').hide();
        $('.image-upload-wrap').show();
    }
    $('.image-upload-wrap').bind('dragover', function () {
        $('.image-upload-wrap').addClass('image-dropping');
    });
    $('.image-upload-wrap').bind('dragleave', function () {
        $('.image-upload-wrap').removeClass('image-dropping');
    });

</script>
@endsection
