@extends('admin-layout.main')

@section('content')

<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>About Us</h1>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <form action="{{ route('about.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <input type="hidden" class="form-control" name="id">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="judul">Judul</label>
                                            <input type="text" class="form-control" name="judul">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="deskripsi">Deskripsi</label>
                                            <textarea name="deskripsi" id="deskripsi"class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <label>Image 1</label>
                                        <div class="file-upload">
                                            <img src="" width="80%" id="photo" class="foto_promo1">
                                            <input class="form-control photo" id="foto" accept="image/*" onchange="readURL(this,$(&quot;.foto_promo1&quot;))" name="img_1" type="file">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <label for="img_2">Image 2</label>
                                        <div class="file-upload">
                                            <img src="" width="80%" id="photo" class="foto_promo2">
                                            <input class="form-control photo" id="foto" accept="image/*" onchange="readURL(this,$(&quot;.foto_promo2&quot;))" name="img_2" type="file">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <label for="img_3">Image 3</label>
                                        <div class="file-upload">
                                            <img src="" width="80%" id="photo" class="foto_promo3">
                                            <input class="form-control photo" id="foto" accept="image/*" onchange="readURL(this,$(&quot;.foto_promo3&quot;))" name="img_3" type="file">
                                        </div>
                                    </div>
                                    <div class="col-12 mt-3">
                                        <input type="submit" class="btn btn-primary" value="Submit">
                                    </div>
                                </div>
                            </div>
                         </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection

@section('js')
<script>

    function readURL(input, photo) {
    if (input.files && input.files[0]) {

        var reader = new FileReader();

        reader.onload = function(e) {
        $('.image-upload-wrap').hide();
        photo.attr('src', e.target.result);
        photo.attr('href', e.target.result);
        $('.file-upload-content').show();
        $('.image-title').html(input.files[0].name);
        // $('.file-upload-image').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);

        } else {
            removeUpload();
        }
    }

    function removeUpload() {
        $('.file-upload-input').replaceWith($('.file-upload-input').clone());
        $('.file-upload-content').hide();
        $('.image-upload-wrap').show();
    }
    $('.image-upload-wrap').bind('dragover', function () {
        $('.image-upload-wrap').addClass('image-dropping');
    });
    $('.image-upload-wrap').bind('dragleave', function () {
        $('.image-upload-wrap').removeClass('image-dropping');
    });

</script>
@endsection
