@extends('admin-layout.main')

@section('content')

<div class="main-content">
    <section class="section">
    <div class="section-header">
        <h1>List Gallery</h1>
    </div>

    <div class="section-body">

        <div class="row">
            @foreach ( $gallery as $gallery )
            <div class="col-3">
                <div class="card">
                    <div class="card-body">
                        <img src="{{asset('assets/img/gallery/'.$gallery->img)}}" alt="" style="width: 100%">
                        <div class="text-center mt-4">
                            <a href="{{route('gallery.delete',$gallery->id)}}" class="btn btn-danger swalDeleteGallery" data-id="{{$gallery->id}}"><i class="fas fa-trash"></i> Hapus</a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="col-3">
                <div class="card">
                    <div class="card-body d-flex justify-content-center">
                        <button class="btn btn-warning createGallery"><i class="fas fa-plus-square"></i> Tambah Gallery</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>

    @include('admin-page.gallery.modal')

</div>

@endsection

@section('js')

<script src="{{asset('stisla/assets/js/page/modules-sweetalert.js')}}"></script>

<script>
    $(document).on('click','.createGallery', function(){

        $('.modal-title').html('Tambah Gallery');
        $('.form-data').prop('method','POST').prop('action','{{route('gallery.store')}}').trigger('reset');
        $("#previewImage").css("display","none");
        $('#modalGallery').modal('show',{ Backdrop:'static'});
        $('#galleryId').val('N');
        $('#submit').show();

    });

    $(document).on('click','.editGallery', function(){

        let id = $(this).data('id');

        $.get('../admin/gallery/edit/'+id+'',function(data){
            $('.modal-title').html('Edit Gallery');
            $('.form-data').prop('method','POST').prop('action','{{route('gallery.store')}}');
            $('#galleryId').val(data.id);
            $('#hidden_image').val(data.img);
            $('#modalGallery').modal('show',{ Backdrop:'static'});
            $("#previewImage").attr("src",'{{ url('../assets/img/gallery') }}/' + data.img).css("display","block");
        });

    });

    $(".swalDeleteGallery").click(function() {

        let id = $(this).data('id');

        swal({
            title: 'Yakin hapus gallery?',
            text: 'Gallery yang sudah dihapus tidak dapat dikembalikan!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
            })
            .then((willDelete) => {
            if (willDelete) {
                window.location ="../admin/gallery/delete/"+id+"",
                swal('Gallery berhasil dihapus!', {
                    icon: 'success',
                });
            } else {
            swal('Gallery masih tersimpan!', {
                icon: 'warning',
            });
            }
        });
    });
</script>

@endsection
