<!-- Modal Detail Menu -->
<div class="modal fade" id="detailInvoice" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-body" style="background-color: var(--primary)">
                <div class="row">
                    <div class="col-12" >
                        <div class="card" id="data-order">
                            <div class="card-body" style="text-center">
                                <h2 style="text-align:center; margin-bottom:0px; color:var(--primary)">Detail Invoice</h2>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <h4 id="reference">No. Referensi</h4>
                                <small id="id"></small>
                                <hr>
                                <span id="status" class="badge badge-success badge-sm text-right">PENDING</span>
                            </div>
                            <div class="card-body">
                                <p class="text-center">
                                    <span class="badge badge-info" id="channel_category">Belum melakukan Pembayaran</span>
                                    <span class="badge badge-info" id="channel_code"></span>
                                </p><hr>
                                <p class="text-center">Net Amount (IDR)</p>
                                <h4 class="text-center" id="net_amount"></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@foreach ($detail_order as $i)
<form action="{{ url('admin/order-admin', $i->order->id) }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PATCH')
    <!-- Modal Terima Pembayaran -->
    <div class="modal fade" id="proses{{ $i->no_resi }}" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                    <div class="modal-header">

                    </div>
                    <div class="modal-body text-center">
                        <h5 class="text-center">
                            Pesanan dengan No. Resi <strong>{{ $i->no_resi }}</strong> akan diteruskan ke Kitchen, <br>
                            Apakah anda Yakin ??
                        </h5>
                        <input type="hidden" name="transaksi_id" value="{{ $i->id }}">
                        <input type="hidden" name="no_resi" value="{{ $i->no_resi }}">
                        <button class="btn btn-info mt-4" type="submit">Proses Pesanan</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endforeach
