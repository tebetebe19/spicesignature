@extends('admin-layout.main')

@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>List Virtual Account</h1>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="table-va">
                                    <thead>
                                        <tr class="text-center">
                                            <th>#</th>
                                            <th>Status</th>
                                            <th>Type</th>
                                            <th>Channel</th>
                                            <th>Account</th>
                                            <th>Amount</th>
                                            <th>Net Amount</th>
                                            <th>Reference</th>
                                            <th>Tanggal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($transaksi as $key => $i)
                                        <tr class="text-center">
                                            <td>
                                                {{ $key+1 }}
                                            </td>
                                            <td>
                                                <span class="badge badge-success">{{ $i['status'] }}</span>
                                            </td>
                                            <td>
                                                {{ $i['type'] }}
                                            </td>
                                            <td>
                                                {{ $i['channel_category'] }}
                                            </td>
                                            <td>
                                                {{ $i['channel_code'] }}
                                            </td>
                                            <td>
                                                @currency($i['amount'])
                                            </td>
                                            <td>
                                                @currency($i['net_amount'])
                                            </td>
                                            <td>
                                                {{ $i['reference_id'] }}
                                            </td>
                                            <td>
                                                {{ date('d-M-Y', strtotime($i['created'])) }}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <td colspan="2" style="font-size: 20px" class="text-right">Total Transaksi</td>
                                        <td colspan="6" style="font-size: 20px">@currency($amount_transaksi)</td>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection

@section('js')

    <script>
        $("#table-va").dataTable({
            "columnDefs": [
                { "sortable": false, "targets": [4] }
            ]
        });
    </script>
@endsection
