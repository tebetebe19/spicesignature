@extends('admin-layout.main')

@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>List Virtual Account</h1>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                            <table class="table table-striped" id="table-va">
                                <thead>
                                <tr class="text-center">
                                    <th>#</th>
                                    <th>Kode Bank</th>
                                    <th>Nama Bank</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($getVABanks as $key => $i)
                                    <tr class="text-center">
                                        <td>
                                            {{ $key+1 }}
                                        </td>
                                        <td class="align-middle">
                                            {{ $i['name'] }}
                                        </td>
                                        <td class="align-middle">
                                            {{ $i['code'] }}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection

@section('js')

    <script>
        $("#table-va").dataTable({
            "columnDefs": [
                { "sortable": false, "targets": [4] }
            ]
        });
    </script>
@endsection
