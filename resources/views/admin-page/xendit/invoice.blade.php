@extends('admin-layout.main')

@section('content')
<div class="main-content">
    <section class="section">

        <div class="section-body">
            <div class="row">
                <div class="col-7">
                    <div class="card">
                        <div class="card-header">
                            <h3></h3>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="table-invoice">
                                    <thead>
                                        <tr class="text-center">
                                            <th>#</th>
                                            <th>ID</th>
                                            <th>Reference ID</th>
                                            <th>No. Resi</th>
                                            <th>Amount</th>
                                            <th>Description</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($invoice as $key => $i)
                                        <tr class="text-center">
                                            <td>
                                                {{ $key+1 }}
                                            </td>
                                            <td>
                                                {{ $i->id }}
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-info text-dark" id="btnInvoice" data-id="{{ $i->external_id }}">{{ $i->external_id }}</button>
                                            </td>
                                            <td>
                                                {{ $i->no_resi }}
                                            </td>
                                            <td>
                                                @currency($i->amount)
                                            </td>
                                            <td>
                                                {{ $i->description }}
                                            </td>
                                            @if ($i->order->status_order == "Pembayaran diterima")
                                            <td>
                                                <button class="btn btn-sm btn-success" disabled>Accepted</button>
                                            </td>
                                            @else
                                            <td>
                                                <button class="btn btn-xs btn-info mt-2" data-toggle="modal" href="#proses{{ $i->no_resi }}">Proses Pesanan</button>
                                            </td>
                                            @endif
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-5">
                    <div class="card">
                        <div class="card-header">
                            <h3>Transaksi Pembayaran</h3>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="table-transaksi">
                                    <thead class="text-center">
                                        <tr>
                                            <th>#</th>
                                            <th>Reference</th>
                                            <th>Amount</th>
                                            <th>Net Amount</th>
                                            <th>Tanggal</th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center">
                                        @foreach ($transaksi as $key => $j)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <button class="btn btn-sm btn-danger btnCopy" id="btnCopy" data-clipboard-target="#input-txt" title="Copy">
                                                            <span class="fas fa-copy"></span>
                                                        </div>
                                                        <input type="text" class="form-control" readonly value="{{ $j['reference_id'] }}" id="input-txt">
                                                    </div>
                                                </div>
                                                </button>
                                            </td>
                                            <td>@currency($j['amount'])</td>
                                            <td>@currency($j['net_amount'])</td>
                                            <td>
                                                {{ date('d-M-Y', strtotime($j['created'])) }}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@include('admin-page.xendit.modal')

@endsection

@section('js')

    <script>
        var clipboard = new ClipboardJS('.btnCopy')
            $(document).on('click', '#btnCopy', function() {
                clipboard.on('success', function(e) {
                console.info('Action:', e.action);
                console.info('Text:', e.text);
                console.info('Trigger:', e.trigger);

                e.clearSelection();
            });

            clipboard.on('error', function(e) {
                console.error('Action:', e.action);
                console.error('Trigger:', e.trigger);
            });
        });

        $("#table-invoice").dataTable({
            "columnDefs": [
                { "sortable": false, "targets": [4] }
            ]
        });
        $("#table-transaksi").dataTable({
            "columnDefs": [
                { "sortable": false, "targets": [4] }
            ]
        });

        $(document).on('click', '#btnInvoice', function(){
            $('#detailInvoice').modal('show');
            var idx = $(this).attr('data-id')
            // console.log(idx);
            var data_order = $('#data-order')

            $.ajax({
                url:"{{ url('admin/xendit/invoice') }}/"+idx,
                method:'get',
                success:function(res){
                    $('#status').text(res.status)
                    $('#reference').text(res.reference_id)
                    $('#channel_category').text(res.channel_category)
                    $('#channel_code').text(res.channel_code)
                    $('#net_amount').text(currency(res.net_amount))
                    // console.log(res)
                },
                error: function(xhr, status, error) {
                    alert("Pengguna belum Melakukan Pembayaran")
                }
            })
            // data_order.html('')
        });

        function currency(val){
        bilangan = val;
        var number_string = bilangan.toString(),
            sisa    = number_string.length % 3,
            rupiah  = number_string.substr(0, sisa),
            ribuan  = number_string.substr(sisa).match(/\d{3}/g);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        return "Rp. "+rupiah
    }

    </script>

@endsection
