@extends('admin-layout.main')

@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>List User</h1>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-5">
                    <div class="card">
                        <div class="card-header">
                            <h4>Petugas Kitchen</h4>
                            <hr>
                            <button type="button" class="btn btn-primary m-b-sm" data-toggle="modal" data-target="#tambah">
                                <i class="fa fa-plus"></i> Tambah Petugas Kitchen
                            </button>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="table-kitchen">
                                    <thead>
                                    <tr class="text-center">
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Terdaftar</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-7">
                    <div class="card">
                        <div class="card-header">
                            <h4>Daftar Pengguna (Reguler Users)</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-user" id="table-user">
                                    <thead>
                                    <tr class="text-center">
                                        <th>#</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Status Verifikasi</th>
                                        <th>Terdaftar</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($pengguna as $key => $i)
                                        <tr class="text-center">
                                            <td>
                                                {{ $key+1 }}
                                            </td>
                                            <td class="align-middle">
                                                {{ $i->name }}
                                            </td>
                                            <td class="align-middle">
                                                {{ $i->email }}
                                            </td>
                                            <td class="align-middle">
                                                @if ($i->email_verified_at == NULL)
                                                <span class="badge badge-sm badge-danger">Belum diverifikasi</span>
                                                @else
                                                <span class="badge badge-sm badge-light">Verified</span>
                                                @endif
                                            </td>
                                            <td class="align-middle">
                                                {{ $i->created_at->format('l, d-M-Y'); }}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@include('admin-page.user.crud')

@endsection

@section('js')
    <script type="text/javascript">
        function showPass(){
            var x = document.getElementById("pass");
            if(x.type === "password"){
                x.type = "text";
            } else {
                x.type = "password";
            }
        }

        function showPassConfirm(){
            var x = document.getElementById("pass_confirm");
            if(x.type === "password"){
                x.type = "text";
            } else {
                x.type = "password";
            }
        }

        $(function(){
            var table = $('#table-kitchen').DataTable({
                processing:true,
                serverSide:true,
                ajax: "{{ route('user.index') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'tanggal', name: 'tanggal'},
                ]
            })
        })
    </script>
    <script>
        $(".table-user").dataTable({
            "columnDefs": [
                { "sortable": false, "targets": [1,5] }
            ]
        });
    </script>

    <script>

        function readURL(input, photo) {
        if (input.files && input.files[0]) {

            var reader = new FileReader();

            reader.onload = function(e) {
            $('.image-upload-wrap').hide();
            photo.attr('src', e.target.result);
            photo.attr('href', e.target.result);
            $('.file-upload-content').show();
            $('.image-title').html(input.files[0].name);
            // $('.file-upload-image').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);

            } else {
                removeUpload();
            }
        }

        function removeUpload() {
            $('.file-upload-input').replaceWith($('.file-upload-input').clone());
            $('.file-upload-content').hide();
            $('.image-upload-wrap').show();
        }
        $('.image-upload-wrap').bind('dragover', function () {
            $('.image-upload-wrap').addClass('image-dropping');
        });
        $('.image-upload-wrap').bind('dragleave', function () {
            $('.image-upload-wrap').removeClass('image-dropping');
        });

    </script>

    <script>
        // var cleaveC = new Cleave('.currency', {
        //     numeral: true,
        //     numeralThousandsGroupStyle: 'thousand'
        // });
    </script>

    @endsection
