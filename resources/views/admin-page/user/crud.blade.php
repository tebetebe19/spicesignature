{{-- Modal Tambah Petugas --}}
<form action="{{ route('user.store') }}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Tambah Data</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" class="form-control" placeholder="Nama Pengguna" name="name">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" placeholder="Email Pengguna" name="email">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" placeholder="Kata Sandi" name="password" id="pass">
                        <input type="checkbox" onclick="showPass()">Show Password
                    </div>
                    <div class="form-group">
                        <label>Ulangi Password</label>
                        <input type="password" class="form-control" placeholder="Ulangi Kata Sandi" name="password_confirmation" id="pass_confirm">
                        <input type="checkbox" onclick="showPassConfirm()">Show Password
                    </div>
                        <input type="hidden" class="form-control" placeholder="Role" name="roles" value="2">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </div>
        </div>
    </div>
</form>
