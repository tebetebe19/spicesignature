@extends('admin-layout.main')

@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>List Menu</h1>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                    <div class="card-header">
                        <button type="button" class="btn btn-primary m-b-sm" data-toggle="modal" data-target="#tambah"><i class="fa fa-plus"></i> Tambah Menu</button>
                    </div>
                    <div class="card-body">
                        @if (count($errors) > 0)
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    @foreach($errors->all() as $error)
                                    {{ $error }} <br>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="table-responsive" style="width: 100%">
                            <table class="table table-striped table-menu" id="tableMenu">
                                <thead>
                                <tr class="text-center">
                                    <th>#</th>
                                    <th>Image</th>
                                    <th>Nama Menu</th>
                                    <th>Harga</th>
                                    <th>Kategori</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody class="text-center">
                                    @foreach ($data as $key => $menu)
                                    <tr class="text-center">
                                        <td>
                                            {{ $key+1 }}
                                        </td>
                                        <td>
                                            <img src="{{asset('assets/admin/menu/'.$menu->img)}}" alt="" style="width: 150px">
                                        </td>
                                        <td class="align-middle">
                                            {{ $menu->nama_menu }}
                                        </td>
                                        <td class="align-middle">
                                            @currency ($menu->harga)
                                        </td>
                                        <td class="align-middle">
                                            {{ $menu->kategori->kategori }}
                                        </td>
                                        <td class="align-middle">
                                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#edit{{$menu->id}}" title="Edit">
                                                <i class="fa fa-pencil"></i> Edit
                                            </button>
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#hapus{{$menu->id}}" title="Hapus">
                                                <i class="fa fa-trash"></i> Hapus
                                            </button>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@include('admin-page.menu.crud')

@endsection

@section('js')
    <script type="text/javascript">
        $("#tableMenu").dataTable({
            "columnDefs": [
                { "sortable": false, "targets": [1,5] }
            ]
        });
    </script>
    <script type="text/javascript">
        // $(function(){
        //     var table = $('#tableMenu').DataTable({
        //         processing:true,
        //         serverSide:true,
        //         ajax: "{{ route('menu.index') }}",
        //         columns: [
        //             {data: 'DT_RowIndex', name: 'DT_RowIndex'},
        //             {data: 'img', name: 'img'},
        //             {data: 'nama_menu', name: 'nama_menu'},
        //             {data: 'harga', name: 'harga'},
        //             {data: 'kategori.kategori', name: 'kategori'},
        //             {
        //                 data: 'action',
        //                 name: 'action',
        //                 orderable: true,
        //                 searchable: true
        //             },
        //         ]
        //     })
        // })
    </script>
    <script>
        $(function(){
            $("form[name='menu']").validate({
                rules: {
                    nama_menu: {
                        required: true,
                        minlength: 5,
                        maxlength: 50
                    },
                    harga: {
                        required: true,
                        number: true
                    },
                    img: {
                        required: true,
                        extension: "jpg|jpeg|png"
                    },
                    desc: {
                        required: true,
                        minlength: 10
                    }
                },
                messages: {
                    nama_menu: {
                        required: "Nama Menu tidak boleh kosong !",
                        minlength: "Nama Menu tidak boleh kurang dari 5 Karakter !",
                        maxlength: "Nama Menu tidak boleh lebih dari 50 Karakter !"
                    },
                    harga: {
                        required: "Harga tidak boleh kosong !",
                        number: "Hanya diperbolehkan Numeric !"
                    },
                    img: {
                        required: "Foto Menu tidak boleh kosong !",
                        extension: "Ekstensi file salah !"
                    },
                    desc: {
                        required: "Deskripsi tidak boleh kosong !",
                        minlength: "Deskripsi tidak boleh kurang dari 10 Karakter !"
                    }
                }
            })
        })
    </script>

    <script>

        function readURL(input, photo) {
        if (input.files && input.files[0]) {

            var reader = new FileReader();

            reader.onload = function(e) {
            $('.image-upload-wrap').hide();
            photo.attr('src', e.target.result);
            photo.attr('href', e.target.result);
            $('.file-upload-content').show();
            $('.image-title').html(input.files[0].name);
            // $('.file-upload-image').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);

            } else {
                removeUpload();
            }
        }

        function removeUpload() {
            $('.file-upload-input').replaceWith($('.file-upload-input').clone());
            $('.file-upload-content').hide();
            $('.image-upload-wrap').show();
        }
        $('.image-upload-wrap').bind('dragover', function () {
            $('.image-upload-wrap').addClass('image-dropping');
        });
        $('.image-upload-wrap').bind('dragleave', function () {
            $('.image-upload-wrap').removeClass('image-dropping');
        });

    </script>

    @endsection
