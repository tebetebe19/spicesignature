{{-- Modal Tambah --}}
<form action="{{ route('menu.store') }}" method="post" enctype="multipart/form-data" name="menu">
    @csrf
    <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Tambah Menu</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Thumbnail</label>
                        <div class="file-upload">
                            <input class="form-control photo" id="foto" accept="image/*" onchange="readURL(this,$(&quot;.foto_promo&quot;))" name="img" type="file">
                        </div>
                        <small id="passwordHelpBlock" class="form-text text-muted">
                            Requirement foto 500x500 px
                        </small>
                    </div>

                    <div class="form-group">
                        <label>Nama Menu</label>
                        <input type="text" class="form-control" placeholder="Nama Menu" name="nama_menu">
                    </div>

                    <div class="form-group">
                        <label>Harga</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                Rp
                                </div>
                            </div>
                            <input type="text" class="form-control" placeholder="Harga" name="harga">
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Kategori</label>
                        <select name="kategori_id" class="form-control" id="kategori_id">
                            @foreach ($kategori as $kat)
                            <option value={{ $kat->id }}>{{ $kat->kategori }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Deskripsi</label>
                        <textarea name="desc" class="form-control"></textarea>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </div>
        </div>
    </div>
</form>
{{-- End of Modal Tambah --}}


{{-- Modal Edit --}}
@foreach ($data as $i)
    <div class="modal fade" id="edit{{ $i->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <form action="{{ url('admin/menu', $i->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Edit Menu</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Thumbnail</label>
                            <div class="file-upload">
                                <img src="{{asset('assets/admin/menu/'.$i->img)}}" width="40%" id="photo" class="foto_promo">
                                <input class="form-control photo" id="foto" accept="image/*" onchange="readURL(this,$(&quot;.foto_promo&quot;))" name="img" type="file">
                                <small id="emailHelp" class="form-text text-muted">Requirement foto 500x500 px</small>

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Nama Menu</label>
                            <input type="text" name="nama_menu" class="form-control" value="{{ $i->nama_menu }}">
                        </div>

                        <div class="form-group">
                            <label>Harga</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                    Rp
                                    </div>
                                </div>
                                <input type="number" class="form-control" name="harga" value="{{ $i->harga }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label>Kategori</label>
                            <select name="kategori_id" id="kategori_id" class="form-control">
                                @foreach ($kategori as $kat)
                                <option value={{ $kat->id }} @if($kat->id == $i->kategori_id) selected @endif>{{ $kat->kategori }}</option>
                                @endforeach

                            </select>
                        </div>
                        <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea name="desc" class="form-control">{{ $i->description }}</textarea>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endforeach
{{-- End of Modal Edit --}}

{{-- Modal Hapus --}}
@foreach ($data as $i)
    <div class="modal fade" id="hapus{{ $i->id }}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <form action="{{ url('admin/menu',$i->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('DELETE')
            <div class="modal-dialog modal-dialog-centered modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="mySmallModalLabel">Hapus Menu</h4>
                    </div>
                    <div class="modal-body text-center">
                        Apa anda yakin ingin menghapus data <br>
                         <b>{{$i->nama_menu}}</b> ?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Ya</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endforeach
{{-- End of Modal Hapus --}}
