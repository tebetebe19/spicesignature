<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>Setting Website</title>

    <!-- General CSS Files -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css" rel="stylesheet">

    <!-- CSS Data Table -->
    <link rel="stylesheet" href="{{asset('stisla/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('stisla/node_modules/datatables.net-select-bs4/css/select.bootstrap4.min.css')}}">

    <!-- Summernote CSS -->
    <link rel="stylesheet" href="{{asset('stisla/node_modules/summernote/dist/summernote-bs4.css')}}">

    <!-- Adv Form Stisla CSS -->
    <link rel="stylesheet" href="{{asset('stisla/node_modules/bootstrap-daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet" href="{{asset('stisla/node_modules/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('stisla/node_modules/select2/dist/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('stisla/node_modules/selectric/public/selectric.css')}}">
    <link rel="stylesheet" href="{{asset('stisla/node_modules/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('stisla/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')}}">

    <!-- Template CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/style-stisla.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/timeline.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/extend-stisla.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/uploadimage.css')}}">
    <link rel="stylesheet" href="{{asset('stisla/assets/css/components.css')}}">

    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>

    <link href='https://api.mapbox.com/mapbox-gl-js/v2.8.1/mapbox-gl.css' rel='stylesheet' />
    <link rel="stylesheet" href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v5.0.0/mapbox-gl-geocoder.css" type="text/css">
    <link rel="stylesheet" href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-directions/v4.1.0/mapbox-gl-directions.css" type="text/css">

</head>

<body>
<div id="app">
    <div class="main-wrapper">

    @include('user-layout.topbar')

    @include('user-layout.sidebar')

    @yield('content')

    @include('user-layout.footer')

    </div>
</div>
{{-- Mapbox --}}
<script src='https://api.mapbox.com/mapbox-gl-js/v2.8.1/mapbox-gl.js'></script>
<script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-directions/v4.1.0/mapbox-gl-directions.js"></script>
<script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v5.0.0/mapbox-gl-geocoder.min.js"></script>
@stack('scripts')

<!-- General JS Scripts -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://js.pusher.com/7.0/pusher.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="{{asset('stisla/assets/js/stisla.js')}}"></script>
<script src="{{asset('assets/js/uploadimage.js')}}"></script>

<!-- JS Libraies -->

<!-- Data Table Libraies -->
<script src="{{asset('stisla/node_modules/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('stisla/node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('stisla/node_modules/datatables.net-select-bs4/js/select.bootstrap4.min.js')}}"></script>
<script src="{{asset('stisla/assets/js/page/modules-datatables.js')}}"></script>

<!-- Summernote JS Libraies -->
<script src="{{asset('stisla/node_modules/summernote/dist/summernote-bs4.js')}}"></script>

<!-- Adv Form Stisla JS Libraies -->
<script src="{{asset('stisla/node_modules/cleave.js/dist/cleave.min.js')}}"></script>
<script src="{{asset('stisla/node_modules/cleave.js/dist/addons/cleave-phone.us.js')}}"></script>
<script src="{{asset('stisla/node_modules/jquery-pwstrength/jquery.pwstrength.min.js')}}"></script>
<script src="{{asset('stisla/node_modules/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('stisla/node_modules/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')}}"></script>
<script src="{{asset('stisla/node_modules/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"></script>
<script src="{{asset('stisla/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
<script src="{{asset('stisla/node_modules/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('stisla/node_modules/selectric/public/jquery.selectric.min.js')}}"></script>
<script src="{{asset('stisla/assets/js/page/forms-advanced-forms.js')}}"></script>

<!-- Template JS File -->
<script src="{{asset('stisla/assets/js/scripts.js')}}"></script>
<script src="{{asset('stisla/assets/js/custom.js')}}"></script>

<script>
    $(document).on('click', '#deleteCart', function(e) {
        swal({
            title: "Almost There!",
            text: "Delete menu?",
            type: "info",
            confirmButtonText: "Yes!",
            showCancelButton: true
        })
            .then((result) => {
            if (result.value) {
                swal(
                'Menu Deleted!',
                'Enjoy your day!',
                'success'
                )
            } else if (result.dismiss === 'cancel') {

            }
        })

    });
</script>

@yield('js')
</body>
</html>
