<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand" style="height: 65px">
            <a href="/">Spice Signature</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="/">SS</a>
        </div>
        <ul class="sidebar-menu">
            <li class="{{ Request::is('*dashboard*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('dashboard-user.index') }}"><i class="fas fa-percentage"></i> <span>Dashboard</span></a>
            </li>
            <li class="{{ Request::is('*cart*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('cart.index') }}"><i class="fas fa-shopping-cart"></i> <span>Cart</span></a>
            </li>
            <li class="{{ Request::is('*order*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('order.index') }}"><i class="fas fa-list"></i> <span>Order</span></a>
            </li>
            <li class="{{ Request::is('*setting-profile*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('setting-profile.index') }}"><i class="fas fa-id-card"></i> <span>Setting Profile</span></a>
            </li>
        </ul>
    </aside>
</div>
