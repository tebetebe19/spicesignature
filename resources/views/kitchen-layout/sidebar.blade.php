<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand" style="height: 65px">
            <a href="/">Spice Signature</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="/">SS</a>
        </div>
        <ul class="sidebar-menu">
            <li class="{{ Request::is('*dashboard*') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('dashboard-kitchen.index')}}"><i class="fas fa-clipboard-list"></i> <span>Dashboard</span></a>
            </li>
            <li class="{{ Request::is('*order*') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('order-kitchen.index')}}"><i class="fas fa-clipboard-list"></i> <span>Order</span></a>
            </li>
        </ul>
    </aside>
</div>
