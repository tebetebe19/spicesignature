<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand" style="height: 65px">
            <a href="/">Spice Signature</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="/">SS</a>
        </div>
        <ul class="sidebar-menu">
            <li class="{{ Request::is('*order*') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('order-admin.index')}}"><i class="fas fa-clipboard-list"></i> <span>Order</span></a>
            </li>
            <li class="menu-header">Web Setting</li>
            <li class="{{ Request::is('*promo*') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('promo.index')}}"><i class="fas fa-percentage"></i> <span>Promo</span></a>
            </li>
            <li class="{{ Request::is('*about*') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('about.index')}}"><i class="fas fa-briefcase"></i> <span>About Us</span></a>
            </li>
            <li class="{{ Request::is('*gallery*') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('gallery.index')}}"><i class="fas fa-images"></i> <span>Gallery</span></a>
            </li>

            <li class="menu-header">Menu</li>
            <li class="{{ Request::is('*menu*') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('menu.index')}}"><i class="fas fa-utensils"></i> <span>Menu</span></a>
            </li>
            <li class="{{ Request::is('*kategori*') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('kategori.index')}}"><i class="fas fa-award"></i> <span>Master Kategori</span></a>
            </li>

            <li class="menu-header">Kelola User</li>
            <li class="{{ Request::is('*user*') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('user.index')}}"><i class="fas fa-user"></i> <span>User</span></a>
            </li>
            <li class="{{ Request::is('*role*') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('role.index')}}"><i class="fas fa-file"></i> <span>Master Role User</span></a>
            </li>
            <li class="menu-header">Xendit Payment Gateway</li>
            {{-- <li class="{{ Request::is('*va*') ? 'active' : '' }}">
                <a class="nav-link" href="{{route('xendit.index')}}"><i class="fas fa-user"></i> <span>List VA</span></a>
            </li> --}}
            <li class="{{ Request::is('*invoice*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('xendit.invoice') }}"><i class="fas fa-clipboard-list"></i> <span>Xendit Invoice</span></a>
            </li>
        </ul>
    </aside>
</div>
