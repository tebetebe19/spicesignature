@extends('user-layout.main')

@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-body">
                @if (session('error'))
                    <div class="alert alert-danger">
                        <strong>{{ session('error') }}</strong>
                    </div>
                @endif

            @if($check)
            <div class="card">
                <div class="card-body">
                    <table class="table table-striped" id="tableOrder">
                        <thead class="text-center">
                            <tr>
                                <th>Provinsi</th>
                                <th>Kota Kab</th>
                                <th>Kecamatan</th>
                                <th>Kelurahan / Desa</th>
                                <th>Detail Alamat</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            @foreach ($data as $i)
                            <tr>
                                <td>{{ $i->villages->district->regency->province->name }}</td>
                                <td>{{ $i->villages->district->regency->name }}</td>
                                <td>{{ $i->villages->district->name }}</td>
                                <td>{{ $i->villages->name }}</td>
                                <td>{{ $i->detail_alamat }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @else
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('shipping-address.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <h3>Alamat Pengiriman</h3>
                        <input type="hidden" name="user_id" value={{ Auth::user()->id }}>
                        <div class="row">
                            <div class="col-12">
                                <div class="row">
                                    <div class="form-group col-6">
                                        <label>Provinsi</label>
                                        <select name="prov" id="prov" class="form-control">
                                            @foreach ($prov as $p)
                                                <option value="{{ $p->id }}">{{ $p->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-6">
                                        <label>Kota / Kabupaten</label>
                                        <select name="kota" id="kota" class="form-control" required >

                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-6">
                                        <label>Kecamatan</label>
                                        <select name="kecamatan" id="kecamatan" class="form-control" required>

                                        </select>
                                    </div>
                                    <div class="form-group col-6">
                                        <label>Kelurahan</label>
                                        <select name="kelurahan" id="kelurahan" class="form-control" required>

                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-12">
                                        <label>Detail Alamat</label>
                                        <textarea name="alamat" id="alamat" cols="30" rows="10" class="form-control" required></textarea>
                                        <span>Nama Jalan / No. Rumah / Nama Gang</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-2">
                                        <label>Latitude</label>
                                        <input type="text" name="lat" id="lat" class="form-control">
                                    </div>
                                    <div class="form-group col-2">
                                        <label>Longitude</label>
                                        <input type="text" name="long" id="long" class="form-control">
                                    </div>
                                    <div class="form-group col-8">
                                        <div id='map' style='width: 100%; height: 500px;'>

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @endif
        </div>
    </section>
</div>
@endsection

@section('js')
@push('scripts')
<script>
    mapboxgl.accessToken = 'pk.eyJ1Ijoic3BpY2VzaWduYXR1cmUiLCJhIjoiY2wzdG1hanYwMjFtcjNicGU2YWRkeWc0cCJ9.vbGzNcWEjq__vqVg8EgAUA';

    var map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v11',
        center: [106.78336105191374, -6.242799250],
        zoom: 16
    });

    var marker = new mapboxgl.Marker();

    // let mapEl = document.getElementById('map');
    // mapEl.addEventListener('click', onClick);

    map.on('click', function(e) {
        var coordinates = e.lngLat;
        // new mapboxgl.Popup()
        //     // .setLng(coordinates.lng)
        //     .setLngLat(coordinates)
        //     .setHTML(coordinates)
        //     .addTo(map);
        var lat = e.lngLat.wrap().lat;
        var lng = e.lngLat.wrap().lng;
        $("input#lat").val(lat);
        $("input#long").val(lng);
        marker.setLngLat(coordinates).addTo(map);

    });

    const marker1 = new mapboxgl.Marker()
    .setLngLat([106.78335050175951, -6.24279334337993])
    .addTo(map);

    map.addControl(
        new MapboxGeocoder({
            accessToken: mapboxgl.accessToken,
            mapboxgl: mapboxgl
        })
    );
</script>
@endpush

<script type="text/javascript">
    jQuery(document).ready(function(){
        //get Kota berdasarkan provinsi
        jQuery('select[name="prov"]').on('change', function()
        {
            var prov = jQuery(this).val();
            if(prov)
            {
                jQuery.ajax({
                    url : '/user/getKota/' +prov,
                    type : "GET",
                    dataType : "json",
                    success:function(data)
                    {
                        jQuery('select[name="kota"]').empty();
                        jQuery.each(data, function(key, value){
                            $('select[name="kota"]').append('<option value="'+ key +'">'+ value + '</option>');
                        });
                    }
                });
            }
            else
            {
                $('select[name="kota"]').empty();
            }
        });

        //get Kecamatan berdasarkan kota
        jQuery('select[name="kota"]').on('change', function()
        {
            var kota = jQuery(this).val();
            if(kota)
            {
                jQuery.ajax({
                    url : '/user/getKecamatan/' +kota,
                    type : "GET",
                    dataType : "json",
                    success:function(data)
                    {
                        jQuery('select[name="kecamatan"]').empty();
                        jQuery.each(data, function(key, value){
                            $('select[name="kecamatan"]').append('<option value="'+ key +'">'+ value + '</option>');
                        });
                    }
                });
            }
            else
            {
                $('select[name="kecamatan"]').empty();
            }
        });

        //get Kelurahan berdasarkan Kecamatan
        jQuery('select[name="kecamatan"]').on('change', function()
        {
            var kecamatan = jQuery(this).val();
            if(kecamatan)
            {
                jQuery.ajax({
                    url : '/user/getKelurahan/' +kecamatan,
                    type : "GET",
                    dataType : "json",
                    success:function(data)
                    {
                        jQuery('select[name="kelurahan"]').empty();
                        jQuery.each(data, function(key, value){
                            $('select[name="kelurahan"]').append('<option value="'+ key +'">'+ value + '</option>');
                        });
                    }
                });
            }
            else
            {
                $('select[name="kelurahan"]').empty();
            }
        });
    })
</script>
@endsection
