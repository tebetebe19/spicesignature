@extends('user-layout.main')

@section('content')
@if ($check->alamat == null)
<div class="main-content">
    <div class="section">
        <div class="section-body">
            <div class="alert alert-danger text-center">
                <strong>Data belum lengkap, Lengkapi Data Profile terlebih dahulu !!</strong>
            </div>
        </div>
    </div>
</div>
@else
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Cart</h1>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-lg-8 col-md-6">
                    <h2 class="section-title" style="font-weight: bolder;">Menu</h2>
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <ul class="nav nav-pills">
                                        @foreach ($kategori as $j)
                                        <li class="nav-item">
                                            <a class="nav-link @if($loop->first) active @endif" data-toggle="pill" href="#{{ $j->kategori }}" role="tab">
                                                {{ $j->kategori }}
                                            </a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="tab-content">
                                {{-- Looping Kategori start --}}
                                @foreach ($kategori as $kat)
                                <div class="tab-pane fade @if($loop->first)show active @endif" id="{{ $kat->kategori }}" role="tabpanel">
                                    <div class="row">
                                        @foreach ($kat->menu as $i)
                                        {{-- Looping Menu berdasarkan Kategori start --}}
                                        <div class="col-lg-6 col-md-12">
                                            <div class="card card-cart">
                                                <div class="card-body">
                                                    <div class="image">
                                                        <img src="{{ asset('assets/admin/menu/'.$i->img)}}" alt="" style="width:100%; object-fit: cover">
                                                    </div>
                                                    <div class="bag">
                                                        <div>
                                                            <div class="title">
                                                                {{ $i->nama_menu }}
                                                            </div>
                                                            <div class="deskripsi">
                                                                {{ $i->description }}
                                                            </div>
                                                            <div class="ammount">
                                                                @currency($i->harga)
                                                            </div>

                                                            <div>
                                                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addToCart{{ $i->id }}">
                                                                    <i class="fas fa-cart-plus"></i> Add To Cart
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                        {{-- Looping Menu berdasarkan Kategori end --}}
                                    </div>
                                </div>
                                @endforeach
                                {{-- Looping Kategori end --}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <h2 class="section-title" style="font-weight: bolder">Cart List</h2>
                    <div class="card" style="background-color: var(--primary)">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body" style="">
                                            <div class="penerima" style="">
                                                <span style="font-weight: bolder; font-size: 30px; color: var(--primary)">{{ Auth::user()->name }}</span>
                                            <div class="alamat">
                                                <i class="fas fa-map-marker-alt mr-2" style="color: red"></i> {{ $shipping->detail_alamat }}
                                            </div>
                                            <div class="phone">
                                                <i class="fas fa-mobile-alt mr-2" style="color: rgb(91, 91, 231)"></i> No. HP : <b>{{ $check->nohp }}</b>
                                            </div>
                                            <div class="phone">
                                                <i class="fab fa-whatsapp" style="color: rgb(91, 231, 184)"></i> Whatsapp : <b>{{ $check->whatsapp }}</b>
                                            </div>
                                            <a class="btn btn-primary mt-2" href="{{ route('setting-profile.index') }}">Edit Profile</a>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                    // $harga = 2000;
                                    // $qty = 5;
                                ?>
                                @foreach ($cartItems as $j)
                                <div class="card card-cart-preview">
                                    <div class="card-body">
                                        <img src="{{ asset('assets/admin/menu/'.$j->attributes->image) }}" alt="" style="">
                                        <div class="bag">
                                            <div>
                                                <div class="summary">
                                                    {{ $j->quantity }} x {{ $j->name }} @ @currency($j->price)
                                                </div>
                                                <div class="total">
                                                    Subtotal : @currency($j->price * $j->quantity)
                                                    <input type="hidden" value="{{ $j->price * $j->quantity }}">
                                                </div>
                                                <form action="{{ route('cart.remove') }}" method="POST">
                                                    @csrf
                                                    <input type="hidden" value="{{ $j->id }}" name="id">
                                                    <button class="btn btn-danger btn-sm mt-3">Delete</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach

                                <div class="card">
                                    <div class="card-body text-right">
                                        Total <b>@currency(Cart::getTotal())</b><br>
                                        Ongkos Kirim <b>@currency($distance*5)</b>
                                        <hr>
                                        Grand Total = <b>@currency(Cart::getTotal() + ($distance*5))</b>
                                    </div>
                                </div>

                                <button class="btn btn-warning" data-toggle="modal" data-target="#checkOut" style="width: 100%">Check Out</button>
                                <form action="{{ route('cart.clear') }}" method="POST">
                                    @csrf
                                    <button class="btn btn-danger btn-md mt-3">Remove All Cart</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@include('user-page.cart.modal')
@include('user-page.cart.modalChange')
@include('user-page.cart.modalCheckOut')

@endif
@endsection


@section('js')
@endsection
