<!-- Modal Change Qty -->
<div class="modal fade modalAddToCart" id="checkOut" tabindex="-1" aria-labelledby="addToCartLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body" style="background-color: var(--primary)">
                <form action="{{ route('order.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-12" >
                            <div class="card">
                                <div class="card-body" style="text-center">
                                    <h2 style="text-align:center; margin-bottom:0px; color:var(--primary)">Review Order</h2>
                                    <h5 style="text-align:center; margin-bottom:0px; color:var(--primary)">
                                        No. Resi : {{ $no_resi }}
                                    </h5>
                                    <input type="hidden" name="no_resi" id="no_resi" value="{{ $no_resi }}">
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <div class="penerima" style="font-weight: bolder; font-size: larger; color: var(--primary)">
                                        {{ Auth::user()->name }}
                                        <input type="hidden" name="user_id" id="user_id" value="{{ Auth::user()->id }}">
                                        <input type="hidden" name="nama" id="nama" value="{{ Auth::user()->name }}">
                                        <input type="hidden" name="email" id="email" value="{{ Auth::user()->email }}">
                                    </div>
                                    <div class="alamat">
                                        <i class="fas fa-map-marker-alt mr-2" style="color: red"></i> {{ $shipping->detail_alamat }}
                                        <input type="hidden" name="alamat" id="alamat" value="{{ $check->alamat }}">
                                    </div>
                                    <div class="phone">
                                        <i class="fas fa-mobile-alt mr-2" style="color: rgb(91, 91, 231)"></i> {{ $check->nohp }} <a href=""><i class="fas fa-pencil-alt ml-4"></i></a>
                                        <input type="hidden" name="nohp" id="nohp" value="{{ $check->nohp }}">
                                        <input type="hidden" name="shipping_id" id="shipping_id" value="{{ $shipping->id }}" class="form-control">
                                        <input type="hidden" name="jarak" id="jarak" value="{{ $distance / 1000 }} Km" class="form-control">
                                    </div>
                                </div>
                            </div>

                            @foreach ($cartItems as $j)
                            <div class="card card-cart-preview">
                                <div class="card-body">
                                    <img src="{{ asset('assets/admin/menu/'.$j->attributes->image) }}" alt="" style="">
                                    <input type="hidden" name="menu_id[]" value="{{ $j->id }}">
                                    <div class="bag">
                                        <div>
                                            <div class="summary mb-2">
                                                {{ $j->quantity }} x {{ $j->name }}
                                                <input type="hidden" name="qty[]" value="{{ $j->quantity }}">
                                            </div>
                                            <div class="total">
                                                Subtotal : @currency($j->price * $j->quantity)
                                                <input type="hidden" name="subtotal[]" value="{{ $j->price * $j->quantity }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            <div class="card">
                                <div class="card-body text-right">
                                    Total <b>@currency(Cart::getTotal())</b><br>
                                    Ongkos Kirim <b>@currency($distance*5)</b>
                                    <hr>
                                    Grand Total = <b>@currency(Cart::getTotal() + ($distance*5))</b>
                                    <input type="hidden" name=ongkir value="{{ $distance*5 }}">
                                    <input type="hidden" name=total value="{{ Cart::getTotal() }}">
                                    <input type="hidden" name=grand_total value="{{ Cart::getTotal() + ($distance*5) }}">
                                </div>
                            </div>
                            <button class="btn btn-warning save_order" type="submit" style="width: 100%">Process</button>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@push('javascript')
{{-- <script>
    $(document).ready(function(){
        $('.save_order').on('click', function(e){
            let
        });
    });
</script> --}}
@endpush
