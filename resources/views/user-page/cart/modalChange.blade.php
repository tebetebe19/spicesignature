
<!-- Modal Change Qty -->
<div class="modal fade modalAddToCart" id="changeQty" tabindex="-1" aria-labelledby="addToCartLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body text-center">
                <img src="{{ asset('assets/img/promo/1.png')}}" alt="" style="width: 100%">
                <div>
                    [Nama Barang] - Rp [999.999.999]/Pax
                </div>
                <div>
                    Orders : <input type="number" value="1" min="1" max="10">
                </div>
                <div>
                    <button class="btn btn-secondary btn-sm" data-bs-dismiss="modal" aria-label="Close">Cancel</button>
                    <button class="btn btn-primary btn-sm">Change Qty</button>
                </div>
            </div>
        </div>
    </div>
</div>

