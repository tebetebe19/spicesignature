
@foreach ($menu as $i)
<div class="modal fade modalAddToCart" id="addToCart{{ $i->id }}" tabindex="-1" aria-labelledby="addToCartLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action="{{ route('cart.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body text-center mt-2">
                    <img src="{{ asset('assets/admin/menu/'.$i->img) }}" alt="" style="width: 100%">
                    <input type="hidden" value="{{ $i->img }}"  name="image">
                    <div class="mt-3">
                        <input type="hidden" value="{{ $i->id }}"  name="id">
                        <input type="hidden" value="{{ $i->nama_menu }}"  name="name">
                        <input type="hidden" value="{{ $i->harga }}"  name="price">
                        {{ $i->nama_menu }} - @currency($i->harga)
                    </div>
                    <div>
                        Orders : <input name="quantity" type="number" value="1" min="1" max="10">
                    </div>
                    <div class="mt-3">
                        <button class="btn {{ Request::is('*user/cart*') ? 'btn-secondary' : 'btn-secondary-ss' }} btn-sm" data-bs-dismiss="modal">Cancel</button>
                        <button class="btn {{ Request::is('*user/cart*') ? 'btn-primary' : 'btn-primary-ss' }} btn-sm">Add To Cart</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endforeach

@section('js')
@endsection
