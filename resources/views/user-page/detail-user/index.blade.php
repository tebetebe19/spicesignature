@extends('user-layout.main')

@section('content')
<div class="main-content">
    <section class="section">
        @if ($data_isExist)
        <div class="section-header">
            Data Pengguna
        </div>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <h3>Profile</h3>
                    <p>Nama : {{ Auth::user()->name }}</p>
                    <p>Email : {{ Auth::user()->email }}</p>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="alamat">Alamat</label>
                                <textarea name="alamat_edit" class="form-control" disabled>{{ $data->alamat }}</textarea>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="nohp">No. Handphone</label>
                                <input type="text" name="nohp_edit" class="form-control" disabled value="{{ $data->nohp }}">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="whatsapp">Whatsapp</label>
                                <input type="text" name="whatsapp_edit" class="form-control" disabled value="{{ $data->whatsapp }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @else
        <div class="section-header">
            <h1>Update Data Pengguna</h1>
        </div>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <form action="#" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <input type="hidden" class="form-control" value="{{ Auth::user()->id }}" name="user_id">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="alamat">Alamat</label>
                                    <textarea name="alamat" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="nohp">No. Handphone</label>
                                    <input type="number" name="nohp" class="form-control">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="whatsapp">Whatsapp</label>
                                    <input type="number" name="whatsapp" class="form-control">
                                </div>
                            </div>
                            <div class="col-12 mt-3">
                                <input type="submit" class="btn btn-primary" value="Simpan">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        @endif
        </div>
    </section>
</div>
@endsection
