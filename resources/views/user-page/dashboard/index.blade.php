@extends('user-layout.main')

@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Dashboard</h1>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-4">
                    <div class="card">
                        <div class="card-header">
                            <h3>Data Order</h3>
                        </div>
                        <div class="card-body">
                            <ul class="list-group">
                                @foreach ($transaksi as $i)
                                <li class="list-group-item">
                                    No. Resi : <a href="#" style="color: royalblue" id="data_riwayat" data-id={{ $i->no_resi }}>{{ $i->no_resi }}</a> -
                                    <span class="pull-right">{{ $i->created_at->isoformat('dddd, D MMMM Y') }}</span>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-8">
                    <div class="card">
                        <div class="card-header text-white bg-info">
                            <h3>Riwayat Pesanan</h3>
                        </div>
                        <div class="card-body" id="riwayat-trans">
                            <ul class="timeline" id="data-order">
                                {{-- Data ditampilkan menggunakan jQuery --}}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection

@section('js')

<script type="text/javascript">

    $(document).on('click', '#data_riwayat', function() {
        var idx = $(this).attr('data-id')
        // console.log(idx);
        var data_order = $('#data-order')
        data_order.html('')
        $.ajax({
            url:"{{ url('user/riwayat') }}/"+idx,
            method:'get',

            success:function(res){
                $('#tanggal').text(res.tanggal)
                $('#jam').text(res.jam)
                $('#deskripsi').text(res.deskripsi)
                // console.log(res)
                $.each(res.riwayat, function(idk,val){
                    data_order.append(`
                        <li>
                            <div class="timeline-badge info text-dark"><i class="${val.icon}"></i></div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 class="timeline-title">${val.tanggal}</h4>
                                    <h5><small class="text-muted"><i class="fas fa-clock"></i> ${val.jam}</small></h5>
                                </div>
                                <div class="timeline-body">
                                    <p>${val.Deskripsi}</p>
                                </div>
                            </div>
                        </li>
                    `)

                })
            },
            error: function(xhr, status, error) {
                // alert(xhr.responseText);
                alert("error")
            }
        })
    })

</script>

@endsection
