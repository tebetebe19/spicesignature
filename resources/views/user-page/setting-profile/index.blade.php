@extends('user-layout.main')

@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            Data Pengguna
        </div>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <form action="{{ url('user/setting-profile', Auth::user()->id) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <h3>Profile</h3>
                        <p>Nama : {{ Auth::user()->name }}</p>
                        <p>Email : {{ Auth::user()->email }}</p>
                        <input type="hidden" name="id" value={{ Auth::user()->id }}>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="alamat">Alamat</label>
                                    @if ($check->alamat == null)
                                    <textarea name="alamat" class="form-control" required></textarea>
                                    @else
                                    <textarea name="alamat" class="form-control">{{ $data->alamat }}</textarea>
                                    @endif
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="nohp">No. Handphone</label>
                                    @if($check->nohp == null)
                                    <input type="text" name="nohp" class="form-control" required>
                                    @else
                                    <input type="text" name="nohp" class="form-control" value="{{ $data->nohp }}">
                                    @endif
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="whatsapp">Whatsapp</label>
                                    @if ($check->whatsapp == null)
                                    <input type="text" name="whatsapp" class="form-control" required>
                                    @else
                                    <input type="text" name="whatsapp" class="form-control" value="{{ $data->whatsapp }}">
                                    @endif
                                </div>
                            </div>
                            <div class="col">
                                <button type="submit" class="btn btn-success">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
