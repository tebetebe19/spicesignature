@extends('user-layout.main')

@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Order List</h1>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="tableOrder">
                                    <thead>
                                    <tr class="text-center">
                                        <th>#</th>
                                        <th>No.Invoice</th>
                                        <th>Order Date</th>
                                        <th>Harga</th>
                                        <th>Menu</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($detail_order as $key => $i)
                                        <tr class="text-center">
                                            <td>
                                                {{ $key+1 }}
                                            </td>
                                            <td class="align-middle">
                                                <a href="" data-toggle="modal" data-target="#timeline{{ $i->no_resi }}" >{{ $i->no_resi }}</a>
                                            </td>
                                            <td class="align-middle">
                                                {{ $i->created_at->isoFormat('d-MMMM-Y') }}
                                            </td>
                                            <td class="align-middle">
                                                @currency($i->total)
                                            </td>
                                            <td class="align-middle">
                                                <button class="btn btn-info" id="btnDetailMenu" data-id="{{ $i->no_resi }}">Detail Menu</button>
                                            </td>
                                            @if ($i->order->status_order == "Pembayaran diterima")
                                            <td class="align-middle" style="color: green; font-weight: bolder">
                                                <span class="badge badge-sm badge-success mb-1">{{ $i->order->status_order }}</span> <br>
                                                Order Ready To Cook
                                            </td>
                                            @elseif ($i->order->status_order == "Pembayaran sedang direview")
                                            <td class="align-middle" style="color: aqua; font-weight: bolder">
                                                <span class="badge badge-sm badge-warning">{{ $i->order->status_order }}</span>
                                            </td>
                                            @elseif($i->order->status_order == "Pesanan telah dikirim" || "Pesanan telah sampai")
                                            <td class="align-middle" style="color: rgb(2, 214, 214); font-weight: bolder">
                                                <span class="badge badge-sm badge-info mb-1">{{ $i->order->status_order }}</span> <br>
                                                Klik 'Detail' untuk informasi kurir
                                            </td>
                                            @else
                                            <td class="align-middle" style="color: red; font-weight: bolder">
                                                <span class="badge badge-sm badge-danger mb-1">{{ $i->order->status_order }}</span> <br>
                                                Mohon upload bukti transfer
                                            </td>
                                            @endif
                                            <td class="align-middle">
                                                @if ($i->order->bukti_transfer == NULL)
                                                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#uploadTF{{ $i->no_resi }}">
                                                    <i class="fa fa-pencil"></i> Upload Bukti Transfer
                                                </button>
                                                @endif
                                                @if ($i->order->status_order == "Pesanan telah dikirim" || "Pesanan telah sampai")
                                                <button type="subimt" class="btn btn-info" title="Konfirmasi Pesanan">
                                                    Detail
                                                </button>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@include('user-page.order.modal')

@endsection

@section('js')
<script type="text/javascript">
    $("#tableOrder").dataTable({
        "columnDefs": [
            { "sortable": false, "targets": [6] }
        ]
    });

    $(document).on('click', '#btnDetailMenu', function() {
        $('#detailMenu').modal('show');
        var idx = $(this).attr('data-id')
        // console.log(idx);
        var data_order = $('#data-order')
        data_order.html('')
        $.ajax({
            url:"{{ url('user/order') }}/"+idx,
            method:'get',
            // console.log(url)
            success:function(res){
                $('#penerima').text(res.user.name)
                $('#phone').text(res.user.nohp)
                $('#alamat').text(res.shipping.detail_alamat)
                $('#ongkir').text(currency(res.ongkir))
                $('#total').text(currency(res.total))
                $('#grand_total').text(currency(res.grand_total))
                // console.log(res)
                $.each(res.detail_transaksi, function(idk,val){
                    sub_total = parseInt(val.menu.harga * val.qty)
                    data_order.append(`
                        <div class="card-body">
                                <img src="{{ asset('assets/admin/menu/${val.menu.img}') }}" alt="" style="">
                                <div class="bag">
                                    <div>
                                        <div class="summary mb-2">
                                            ${val.qty} x ${val.menu.nama_menu }
                                        </div>
                                        <div class="total">
                                            Subtotal :  ${currency(sub_total)}
                                        </div>
                                    </div>
                                </div>
                            </div>
                    `)

                })
            },
            error: function(xhr, status, error) {
                // alert(xhr.responseText);
                alert("error")
            }
        })
    })
    function currency(val){
        bilangan = val;
        var number_string = bilangan.toString(),
            sisa    = number_string.length % 3,
            rupiah  = number_string.substr(0, sisa),
            ribuan  = number_string.substr(sisa).match(/\d{3}/g);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        return "Rp. "+rupiah
    }
</script>
@endsection
