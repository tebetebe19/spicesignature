<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role')->insert([
        	'name'=> 'Admin',
        ]);
        DB::table('role')->insert([
        	'name'=> 'Kitchen',
        ]);
        DB::table('role')->insert([
        	'name'=> 'Member    ',
        ]);
    }
}
