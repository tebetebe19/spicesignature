<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddVaToOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order', function (Blueprint $table) {
            $table->string('owner_id')->after('status_order');
            $table->string('no_va')->after('owner_id');
            $table->string('bank_code')->after('no_va');
            $table->string('merchant_code')->after('bank_code');
            $table->string('no_akun')->after('merchant_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order', function (Blueprint $table) {
            $table->dropColumn('owner_id');
            $table->dropColumn('no_va');
            $table->dropColumn('bank_code');
            $table->dropColumn('merchant_code');
            $table->dropColumn('no_akun');
        });
    }
}
