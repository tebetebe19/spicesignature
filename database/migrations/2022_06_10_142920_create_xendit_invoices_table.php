<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateXenditInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xendit_invoice', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('external_id');
            $table->string('user_id');
            $table->string('status');
            $table->string('merchant_name');
            $table->double('amount');
            $table->string('description');
            $table->dateTime('expiry_date');
            $table->string('invoice_url');
            $table->string('customer_email');
            $table->string('customer_mobile_number');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xendit_invoice');
    }
}
