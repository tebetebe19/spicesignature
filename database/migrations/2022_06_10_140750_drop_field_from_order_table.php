<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropFieldFromOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order', function (Blueprint $table) {
            $table->dropColumn('owner_id');
            $table->dropColumn('no_va');
            $table->dropColumn('channel');
            $table->dropColumn('status');
            $table->dropColumn('payment_id');
            $table->dropColumn('bank_code');
            $table->dropColumn('merchant_code');
            $table->dropColumn('exp_date');
            $table->dropColumn('amount');
            $table->dropColumn('is_closed');
            $table->dropColumn('no_akun');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order', function (Blueprint $table) {
            //
        });
    }
}
