<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\VisitorController;
use App\Http\Controllers\Payment\XenditController;

// Admin Controller
use App\Http\Controllers\Admin\MenuController;
use App\Http\Controllers\Admin\AboutController;
use App\Http\Controllers\Admin\PromoController;
use App\Http\Controllers\Admin\GalleryController;
use App\Http\Controllers\Admin\KategoriController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\OrderAdminController;

// User Controller
use App\Http\Controllers\User\DashboardUserController;
use App\Http\Controllers\User\CartController;
use App\Http\Controllers\User\OrderController;
use App\Http\Controllers\User\SettingProfileController;
use App\Http\Controllers\User\ShippingAddressController;

// Kitchen Controller
use App\Http\Controllers\Kitchen\DashboardKitchenController;
use App\Http\Controllers\Kitchen\OrderKitchenController;

use App\Http\Controllers\Kurir\KurirController;

Route::get('/', [VisitorController::class, 'index'])->name('visitor.index');
Route::view('/login', 'visitor-page.login');
Route::get('xendit/va/list', [XenditController::class, 'getVirtualAccount'])->name('xendit.index');
Route::post('xendit/va/callback', [XenditController::class, 'callbackVa']);

//ROUTE untuk ADMIN
// CheckRole 1 untuk Admin
Route::group(['middleware' => ['auth','checkRole:1']], function () {
    Route::prefix('admin')->group(function () {
        Route::resource('role', RoleController::class)->except(['create', 'edit']);
        Route::resource('menu', MenuController::class)->except(['create', 'edit']);
        Route::resource('kategori', KategoriController::class)->except(['create', 'edit']);
        Route::resource('promo', PromoController::class)->except(['create', 'edit']);
        Route::resource('user', UserController::class);
        Route::resource('order-admin', OrderAdminController::class)->only(['index', 'update']);
        Route::get('order-admin/{no_resi}','App\Http\Controllers\Admin\OrderAdminController@getDetail')->name('detail-order');
        Route::resource('about', AboutController::class)->except('destroy');
        Route::get('xendit/va/transaksi', [XenditController::class, 'getTransaksi']);
        Route::get('xendit/invoice', [XenditController::class, 'getInvoice'])->name('xendit.invoice');
        Route::get('xendit/invoice/{external_id}', [XenditController::class, 'detailInvoice'])->name('xendit.detail-invoice');
        Route::prefix('gallery')->group(function () {
            Route::get('/', [GalleryController::class, 'index'])->name('gallery.index');
            Route::post('/store', [GalleryController::class, 'store'])->name('gallery.store');
            Route::get('/delete/{id}', [GalleryController::class, 'destroy'])->name('gallery.delete');
        });
    });
});

// ROUTE untuk KITCHEN
// CheckRole 2 untuk Kitchen
Route::group(['middleware' => ['auth', 'checkRole:2']], function () {
    Route::prefix('kitchen')->group(function () {
        Route::resource('dashboard-kitchen', DashboardKitchenController::class);
        Route::resource('order-kitchen', OrderKitchenController::class);
        Route::get('order/{no_resi}','App\Http\Controllers\Kitchen\OrderKitchenController@getDetail')->name('kitchen-order');
        Route::resource('kurir', KurirController::class);
        Route::match(['put', 'patch'],'status_kurir/{no_resi}', 'App\Http\Controllers\Kurir\KurirController@status_kurir')->name('status_kurir');
    });
});

// ROUTE untuk MEMBER
// CheckRole 3 untuk Member
Auth::routes(['verify' => true]);
Route::group(['middleware' => ['auth', 'verified','checkRole:3']], function (){
    Route::prefix('user')->group(function () {
        Route::resource('dashboard-user', DashboardUserController::class);
        Route::resource('setting-profile', SettingProfileController::class);
        Route::get('cart-list', [CartController::class, 'index'])->name('cart.index');
        Route::post('cart-list', [CartController::class, 'addToCart'])->name('cart.store');
        Route::post('update-cart', [CartController::class, 'updateCart'])->name('cart.update');
        Route::post('remove-cart', [CartController::class, 'removeCart'])->name('cart.remove');
        Route::post('clear-cart', [CartController::class, 'clearAllCart'])->name('cart.clear');
        Route::get('order/{no_resi}','App\Http\Controllers\User\OrderController@getDetail')->name('order-detail');
        Route::get('riwayat/{no_resi}','App\Http\Controllers\User\DashboardUserController@getRiwayat')->name('riwayat-order');
        Route::resource('order', OrderController::class);
        Route::resource('shipping-address', ShippingAddressController::class);
        Route::get('/getKota/{id}', [ShippingAddressController::class, 'getKota'])->name('getKota');
        Route::get('/getKecamatan/{id}', [ShippingAddressController::class, 'getKecamatan'])->name('getKecamatan');
        Route::get('/getKelurahan/{id}', [ShippingAddressController::class, 'getKelurahan'])->name('getKelurahan');
    });
});
