<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\MenuController;
use App\Http\Controllers\API\OrderController;
use App\Http\Controllers\API\DetailUserController;
use App\Http\Controllers\API\VerificationApiController;
use App\Http\Controllers\API\DetailTransaksiController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);
Route::get('email/verify/{id}', [VerificationApiController::class, 'verify'])->name('verificationapi.verify');
Route::get('email/resend', [VerificationApiController::class, 'resend'])->name('verificationapi.resend');

Route::group(['middleware' => ['auth:api']], function (){
    Route::get('order', [OrderController::class, 'index'])->name('order.index');
    Route::post('order', [OrderController::class, 'store'])->name('order.store');
    Route::get('detail_user', [DetailUserController::class, 'index'])->name('detail_user.index');
    // Update data user
    Route::post('update_user', [AuthController::class, 'update'])->name('user.update');
    //Get Data Transaksi by User Login
    Route::get('transaksi', [DetailTransaksiController::class, 'index'])->name('transaksi.index');
    //Get Detail Transaksi by No. Resi
    Route::get('detail/{no_resi}', [DetailTransaksiController::class, 'detail'])->name('detail_transaksi.detail');
    //Simpan Data Transaksi
    Route::post('save_transaksi', [DetailTransaksiController::class, 'save_transaksi'])->name('detail_transaksi.save_transaksi');

    Route::post('upload_bukti/{id}', [OrderController::class, 'upload_bukti'])->name('order.upload_bukti');
});

Route::get('menu', [MenuController::class, 'index'])->name('menu.index');
Route::get('menu/{id}', [MenuController::class, 'show'])->name('menu.show');

// Route::resource('promo', App\Http\Controllers\API\PromoController::class);
// Route::resource('gallery', App\Http\Controllers\API\GalleryController::class);
