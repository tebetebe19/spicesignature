<?php

namespace App\Mail;

use App\Models\Transaksi;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class OrderMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = Transaksi::where('user_id', Auth::id())->with('user')->latest()->first();
        // $transaksi = Transaksi::where('user_id', Auth::id())->with([
        //                     'detail_transaksi' => function($query) {
        //                         return $query->with([
        //                             'menu' => function($q) {
        //                                 return $q->with('kategori');
        //                             }
        //                         ]);
        //                     }
        //                 ])->get();
        // dd($transaksi->detail_transaksi);
        return $this->view('vendor.mail.invoice-mail', compact('data'));
    }
}
