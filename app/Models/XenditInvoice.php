<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class XenditInvoice extends Model
{
    use HasFactory;
    protected $table = 'xendit_invoice';
    protected $primaryKey = 'id';

    public $incrementing = false;
    protected $dates = ['expiry_date'];
    protected $fillable = [
        'id',
        'external_id',
        'user_id',
        'status',
        'merchant_name',
        'amount',
        'description',
        'expiry_date',
        'invoice_url',
        'customer_email',
        'customer_mobile_number'
    ];

    public function order()
    {
        return $this->hasOne(Order::class, 'no_resi', 'no_resi');
    }

}
