<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kurir extends Model
{
    use HasFactory;
    protected $table = 'kurir';

    protected $fillable = [
        'no_resi','name', 'no_plat', 'link_track'
    ];

    public function transaksi()
    {
        return $this->belongsTo(Transaksi::class, 'no_resi','no_resi');
    }

    public function antrian_order()
    {
        return $this->belongsTo(AntrianOrder::class, 'no_resi', 'no_resi');
    }
}
