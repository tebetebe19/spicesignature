<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AntrianOrder extends Model
{
    use HasFactory;

    protected $table = 'antrian_order';
    protected $fillable = [
        'no_antrian', 'transaksi_id', 'no_resi'
    ];
    protected $dates = ['start', 'finish', 'created_at', 'updated_at'];

    public function transaksi()
    {
        return $this->hasOne(Transaksi::class, 'no_resi', 'no_resi');
    }

    public function kurir()
    {
        return $this->hasOne(Kurir::class, 'no_resi', 'no_resi');
    }
}
