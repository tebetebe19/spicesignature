<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menu';

    protected $fillable = [
        'img', 'nama_menu', 'harga', 'description'
    ];

    public function kategori()
    {
        return $this->belongsTo('App\Models\Kategori', 'kategori_id', 'id');
    }

    public function detail_transaksi()
    {
    	return $this->hasMany('App\Models\DetailTransaksi','menu_id','id');
    }

}
