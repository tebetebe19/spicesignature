<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\DetailTransaksi;

class Transaksi extends Model
{
    use HasFactory;

    protected $table = 'transaksi';
    protected $fillable = [
        'no_resi', 'user_id', 'total'
    ];
    protected $dates = ['created_at'];

    public function order()
    {
        return $this->hasOne(Order::class, 'no_resi', 'no_resi');
    }

    public function detail_transaksi(){
        return $this->hasMany(DetailTransaksi::class,'no_resi','no_resi');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function antrian_order()
    {
        return $this->belongsTo(AntrianOrder::class, 'no_resi', 'no_resi');
    }

    public function kurir()
    {
        return $this->hasOne(Kurir::class, 'no_resi', 'no_resi');
    }

    public function riwayat()
    {
        return $this->hasMany(Riwayat::class, 'no_resi', 'no_resi');
    }

    public function shipping()
    {
        return $this->belongsTo(ShippingAddress::class, 'shipping_id', 'id');
    }
}
