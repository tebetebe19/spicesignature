<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShippingAddress extends Model
{
    use HasFactory;
    protected $table = 'shipping_address';

    protected $fillable = [
        'village_id', 'detail_alamat', 'lat', 'long'
    ];
    public function villages()
    {
        return $this->belongsTo(Village::class);
    }
}
