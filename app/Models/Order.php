<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $table = 'order';

    protected $fillable = [
        'no_resi'
    ];

    // public function user()
    // {
    //     return $this->belongsTo('App\Models\User', 'user_id', 'id');
    // }

    public function transaksi()
    {
        return $this->belongsTo(Transaksi::class, 'no_resi', 'no_resi');
    }

    public function xendit_invoice()
    {
        return $this->belongsTo(XenditInvoice::class, 'no_resi', 'no_resi');
    }
}
