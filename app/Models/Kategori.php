<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    use HasFactory;

    protected $table = 'kategori';

    protected $fillable = [
        'kategori'
    ];

    public function menu()
    {
    	return $this->hasMany('App\Models\Menu','kategori_id','id');
    }

}
