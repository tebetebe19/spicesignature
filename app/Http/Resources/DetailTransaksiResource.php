<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DetailTransaksiResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'no_resi' => $this->no_resi,
            'menu' => $this->detail_transaksi->menu->nama_menu,
            'kategori' => $this->menu->kategori->kategori,
            'deskripsi' => $this->menu->description,
            'qty' => $this->qty,
            'harga' => $this->menu->harga,
            'subtotal' => $this->subtotal
        ];
    }
}
