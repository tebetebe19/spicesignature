<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'no_resi' => $this->no_resi,
            'name' => $this->user->name,
            'email' => $this->user->email,
            'alamat' => $this->user->alamat,
            'nohp' => $this->user->nohp,
            'total' => $this->total,
            'status_order' => $this->order->status_order,
        ];
    }
}
