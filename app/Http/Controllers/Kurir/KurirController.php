<?php

namespace App\Http\Controllers\Kurir;

use Carbon\Carbon;
use App\Models\Kurir;
use App\Models\AntrianOrder;
use App\Models\Riwayat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class KurirController extends Controller
{
    public function store(Request $request)
    {

    }

    public function update(Request $request, $no_resi)
    {
        $tgl = Carbon::now()->format('Y-m-d');
        $jam = Carbon::now()->format('H:i:s');
        $desc = "Pesanan dari Nomor Resi ". $request->no_resi ." Sedang diantar oleh Kurir : " . $request->name . " - (" . $request->no_plat . ")";
        $icon = "fa fa-shipping-fast";

        $riwayat = new Riwayat();
        $riwayat->no_resi = $request->no_resi;
        $riwayat->tanggal = $tgl;
        $riwayat->jam = $jam;
        $riwayat->deskripsi = $desc;
        $riwayat->icon = $icon;
        $riwayat->save();

        $status = "Sedang dalam Perjalanan";
        $stts_order = "Pesanan telah dikirim";

        $kurir = new Kurir();
        $kurir->no_resi = $request->no_resi;
        $kurir->name = $request->name;
        $kurir->no_plat = $request->no_plat;
        $kurir->link_track = $request->link_track;
        $kurir->status = $status;
        $kurir->save();

        $now = Carbon::now();

        DB::table('antrian_order')
            ->where('no_resi', $request->no_resi)
            ->update(['finish' => $now]);

        DB::table('order')
            ->where('no_resi', $request->no_resi)
            ->update(['status_order' => $stts_order]);

        return redirect()->route('order-kitchen.index')->with([
            alert()->success('Sukses', 'Orderan berhasil diteruskan ke Kurir')
        ]);
    }

    public function status_kurir(Request $request, $no_resi)
    {
        $tgl = Carbon::now()->format('Y-m-d');
        $jam = Carbon::now()->format('H:i:s');
        $desc = "Pesanan dari Nomor Resi ". $request->no_resi ." telah sampai, Selamat Menikmati...";
        $icon = "fa fa-check-double";
        $stts_order = "Pesanan telah sampai.";
        $riwayat = new Riwayat();
        $riwayat->no_resi = $request->no_resi;
        $riwayat->tanggal = $tgl;
        $riwayat->jam = $jam;
        $riwayat->deskripsi = $desc;
        $riwayat->icon = $icon;
        $riwayat->save();

        DB::table('order')
            ->where('no_resi', $request->no_resi)
            ->update(['status_order' => $stts_order]);

        DB::table('kurir')
            ->where('no_resi', $request->no_resi)
            ->update(['status' => $request->status_kurir]);
        return redirect()->route('order-kitchen.index')->with([
            alert()->success('Sukses', 'Orderan telah sampai')
        ]);
    }
}
