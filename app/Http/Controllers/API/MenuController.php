<?php

namespace App\Http\Controllers\API;

use Validator;
use App\Models\Menu;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\MenuResource as MenuResource;
use App\Http\Controllers\API\BaseController as BaseController;

class MenuController extends BaseController
{
    public function index()
    {
        $menu = Menu::all();
        return $this->handleResponse(MenuResource::collection($menu), 'Data Menu Berhasil Ditarik');
    }

    // public function store (Request $request)
    // {
    //     $input = $request->all();
    //     $validator = Validator::make($input, [
    //         'img' => 'required',
    //         'nama_menu' => 'required',
    //         'harga' => 'required',
    //         'kategori' => 'required'
    //     ]);
    //     if($validator->fails()){
    //         return $this->handleError($validator->errors());
    //     }
    //     $menu = Menu::create($input);
    //     return $this->handleResponse(new MenuResource($menu), 'Success');
    // }

    public function show($id)
    {
        $menu = Menu::find($id);
        if(is_null($menu)){
            return $this->handleError('Data tidak ditemukan');
        }
        return $this->handleResponse(new MenuResource($menu), 'Success');
    }

    // public function update(Request $request, Menu $menu)
    // {
    //     $input = $request->all();

    //     $validator = Validator::make($input, [
    //         'nama_menu' => 'required',
    //         'harga' => 'required',
    //         'kategori' => 'required',
    //     ]);

    //     if($validator->fails()){
    //         return $this->handleError($validator->errors());
    //     }

    //     $menu->nama_menu = $input['nama_menu'];
    //     $menu->harga = $input['harga'];
    //     $menu->kategori = $input['kategori'];
    //     $menu->save();

    //     return $this->handleResponse(new MenuResource($menu), 'Success');
    // }

    // public function destroy(Menu $menu)
    // {
    //     $menu->delete();
    //     return $this->handleResponse([], 'Success');
    // }
}
