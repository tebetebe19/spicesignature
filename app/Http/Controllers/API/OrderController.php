<?php

namespace App\Http\Controllers\API;

use Validator;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Order;
use App\Models\Riwayat;
use App\Models\Transaksi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\OrderResource as OrderResource;
use App\Http\Controllers\API\BaseController as BaseController;

class OrderController extends BaseController
{
    public function index()
    {
        $order = Transaksi::where('user_id', Auth::id())->with(['order', 'user'])->get();
        return $this->handleResponse(OrderResource::collection($order), 'Data Orderan Berhasil Ditarik');
    }

    public function store(Request $request)
    {
        $input_transaksi = $request->all();
        $validator_transaksi = Validator::make($input_transaksi, [
            'no_resi' => 'required',
            'user_id' => 'required',
            'total' => 'required'
        ]);

        $input_order = $request->all();
        $validator_order = Validator::make($input_transaksi, [
            'no_resi' => 'required',
            'status_order' => 'required'
        ]);

        if($validator_transaksi->fails()){
            return $this->handleError($validator_transaksi->errors());
        }

        if($validator_order->fails()){
            return $this->handleError($validator_order->errors());
        }
        dd($request->all());
        Mail::to($request->email)->send(new OrderMail());

        if(Mail::failures()){
            return response()->Fail('Sorry! Please try again latter');
        } else {
            // return response()->success('Great! Successfully send in your mail');
            \Cart::clear();
            return $this->handleResponse(new MenuResource($menu), 'Success');
        }
    }

    public function upload_bukti(Request $request, $id)
    {
        $tgl = Carbon::now()->format('Y-m-d');
        $jam = Carbon::now()->format('H:i:s');
        $desc = "Nomor Resi ". $request->no_resi ." Telah upload bukti transfer";
        $icon = "fa fa-upload";
        // dd($request->all());
        $riwayat = new Riwayat();
        $riwayat->no_resi = $request->no_resi;
        $riwayat->tanggal = $tgl;
        $riwayat->jam = $jam;
        $riwayat->deskripsi = $desc;
        $riwayat->icon = $icon;
        $riwayat->save();


        $file = $request->file('bukti_transfer');
        $name = time();
        $extension = $file->getClientOriginalExtension();
        $newName = $name . '.' . $extension;
        $path =  $file->move('assets/admin/bukti_transfer',$newName);

        $statusTF = "Pembayaran sedang direview";
        $upload = Order::firstWhere('id', $id);

        // dd($upload);
        $upload->status_order = $statusTF;
        $upload->bukti_transfer = $newName;
        $upload->save();

        return response(['order' => $upload], 201);
    }
}
