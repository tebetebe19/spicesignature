<?php

namespace App\Http\Controllers\API;

use Mail;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Order;
Use App\Mail\OrderMail;
use App\Models\Riwayat;
use App\Models\Transaksi;
use Illuminate\Http\Request;
use App\Models\DetailTransaksi;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Http\Resources\TransaksiResource as TransaksiResource;
use App\Http\Resources\DetailTransaksiResource as DetailTransaksiResource;

class DetailTransaksiController extends BaseController
{

    public function index()
    {
        $transaksi = Transaksi::where('user_id', Auth::id())->with(['user', 'order'])->get();
        return $this->handleResponse(TransaksiResource::collection($transaksi), 'Data Transaksi Berhasil Ditarik');
    }

    public function save_transaksi(Request $request)
    {
        $status_awal = "Menunggu Pembayaran";
        $check = User::orderBy('id', 'ASC')
                    ->where('users.id', Auth::id())
                    ->first();
        $user_id = Auth::id();
        // die($check->email);
        if($check->alamat == null) {
            return response(['failed' => "Lengkapi data terlebih dahulu !"], 301);
        } else {
            foreach($request->menu_id as $key => $menu_id) {
                $detail = new DetailTransaksi();
                $detail->no_resi = $request->no_resi;
                $detail->menu_id = $menu_id;
                $detail->qty = $request->qty[$key];
                $detail->subtotal = $request->subtotal[$key];
                $detail->save();
            }

            $transaksi = new Transaksi();
            $transaksi->no_resi = $request->no_resi;
            $transaksi->user_id = $user_id;
            $transaksi->total = $request->total;
            $transaksi->save();

            $order = new Order();
            $order->no_resi = $request->no_resi;
            $order->status_order = $status_awal;
            $order->save();

            $tgl = Carbon::now()->format('Y-m-d');
            $jam = Carbon::now()->format('H:i:s');
            $desc = "Order menu dengan Nomor Resi ". $request->no_resi;
            $icon = "fa fa-shopping-cart";

            $riwayat = new Riwayat();
            $riwayat->no_resi = $request->no_resi;
            $riwayat->tanggal = $tgl;
            $riwayat->jam = $jam;
            $riwayat->deskripsi = $desc;
            $riwayat->icon = $icon;
            $riwayat->save();

            Mail::to($check->email)->send(new OrderMail());
            $pesan = "Invoice berhasil dikirim ke Email Anda";

            if(Mail::failures()) {
                return response()->Fail('Gagal mengirim Invoice !');
            } else {
                \Cart::clear();
                return response(['detail' => $detail, 'transaksi' => $transaksi, 'order' => $order, 'riwayat' => $riwayat, 'pesan' => $pesan], 201);
            }
        }
    }

    public function detail(Request $request, $no_resi)
    {
        $data = Transaksi::where('no_resi',$no_resi)->with([
            'detail_transaksi'=> function($query){
                return $query->with([
                    'menu'=> function($q){
                        return $q->with('kategori');
                    }
                ]);
            }
            ,'user', 'order'])->get();
        return response(['transaksi' => $data], 201);
    }
}
