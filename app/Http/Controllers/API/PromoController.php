<?php

namespace App\Http\Controllers\API;

use Validator;
use App\Models\Promo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PromoResource as PromoResource;
use App\Http\Controllers\API\BaseController as BaseController;

class PromoController extends BaseController
{
    public function index()
    {
        $promo = Promo::all();
        return $this->handleResponse(PromoResource::collection($promo), 'Success');
    }

    // public function store (Request $request)
    // {
    //     $input = $request->all();
    //     $validator = Validator::make($input, [
    //         'img' => 'required',
    //         'nama_promo' => 'required',
    //         'deskripsi' => 'required'
    //     ]);
    //     if($validator->fails()){
    //         return $this->handleError($validator->errors());
    //     }
    //     $promo = Promo::create($input);
    //     return $this->handleResponse(new PromoResource($promo), 'Success');
    // }

    public function show($id)
    {
        $promo = Promo::find($id);
        if(is_null($promo)){
            return $this->handleError('Data tidak ditemukan');
        }
        return $this->handleResponse(new PromoResource($promo), 'Success');
    }

    // public function update(Request $request, Promo $promo)
    // {
    //     $input = $request->all();
    //     // dd($promo->nama_promo);

    //     $validator = Validator::make($input, [
    //         'nama_promo' => 'required',
    //         'deskripsi' => 'required'
    //     ]);

    //     if($validator->fails()){
    //         return $this->handleError($validator->errors());
    //     }

    //     $promo->nama_promo = $input['nama_promo'];
    //     $promo->deskripsi = $input['deskripsi'];
    //     $promo->save();

    //     return $this->handleResponse(new PromoResource($promo), 'Success');
    // }

    // public function destroy(Promo $promo)
    // {
    //     $promo->delete();
    //     return $this->handleResponse([], 'Data berhasil dihapus');
    // }
}
