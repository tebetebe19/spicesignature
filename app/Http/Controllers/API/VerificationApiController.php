<?php

namespace App\Http\Controllers\API;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Auth\VerifiesEmails;

class VerificationApiController extends Controller
{
    use VerifiesEmails;

    public function verify(Request $request)
    {
        $userID = $request['id'];
        $date = date('Y-m-d g:i:s');

        $user = User::findOrFail($userID);
        $user->email_verified_at = $date;
        $user->save();

        return response()->json('Email Verified');
    }

    public function resend(Request $request)
    {
        if($request->user()->hasVerifiedEmail()) {
            return response()->json('User already have verified email !', 422);
        }

        $request->user()->sendEmailVerificationNotification();

        return response()->json('The notification has been resubmitted');
    }
}
