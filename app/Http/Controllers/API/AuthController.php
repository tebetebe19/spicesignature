<?php

namespace App\Http\Controllers\API;

use Validator;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Auth\VerifiesEmails;
use App\Http\Resources\AuthResource as AuthResource;
use App\Http\Controllers\API\BaseController as BaseController;

class AuthController extends BaseController
{
    use VerifiesEmails;
    public $successStatus = 200;

    public function register(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:55',
            'email' => 'email|required|unique:users',
            'password' => 'required|confirmed',
            'role_id' => 'required'
        ]);

        $validatedData['password'] = Hash::make($request->password);
        $validatedData['role_id'] = 3;

        $user = User::create($validatedData);
        $user->sendApiEmailVerificationNotification();

        $accessToken = $user->createToken('authToken')->accessToken;

        $success['message'] = 'Please confirm yourself by clicking on verify user button sent to you on your email';

        return response(['user' => $user, 'success'=>$success, 'access_token' => $accessToken], $this->successStatus);
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'email|required',
            'password' => 'required'
        ]);
        $credentials = request(['email','password']);

        if (!auth()->attempt($credentials)) {
            return response([
                'message' => 'This User does not exist, check your details'
            ], 400);
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;

        $token = User::where('email','=',$request->email);

        // $getToken = $request->token;

        $user = Auth::user();
        // die($user);
        $response = [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'email_verified_at' => $user->email_verified_at,
            'role' => $user->role->name,
            'created_at' => $user->created_at,
            'updated_at' => $user->updated_at,
            'token' => $accessToken
        ];

        return response()->json([
            'user' => $response,
        ], 200);

    }

    public function update(Request $request)
    {
        $data = $request->all();

        $update = User::firstWhere('id', Auth::id());

        $update->alamat = $request->alamat;
        $update->nohp = $request->nohp;
        $update->whatsapp = $request->whatsapp;

        $update->save();
        $pesan = "Data berhasil diupdate";

        return response(['detail_user' => $update, 'success' => $pesan], 200);
    }
}
