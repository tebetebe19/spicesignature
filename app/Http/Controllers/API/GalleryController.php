<?php

namespace App\Http\Controllers\API;

use Validator;
use App\Models\Gallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\GalleryResource as GalleryResource;
use App\Http\Controllers\API\BaseController as BaseController;

class GalleryController extends BaseController
{
    public function index()
    {
        $galleries = Gallery::all();
        return $this->handleResponse(GalleryResource::collection($galleries), 'Data Gallery Berhasil Ditarik');
    }

    public function store (Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function update(Request $request, Menu $menu)
    {
        //
    }

    public function destroy(Menu $menu)
    {
        //
    }
}
