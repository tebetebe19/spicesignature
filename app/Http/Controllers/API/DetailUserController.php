<?php

namespace App\Http\Controllers\API;

use App\Models\DetailUser;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\DetailUserResource as DetailUserResource;
use App\Http\Controllers\API\BaseController as BaseController;

class DetailUserController extends BaseController
{
    public function index()
    {
        $user = Auth::user();
        // die($user);
        $response = [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'email_verified_at' => $user->email_verified_at,
            'alamat' => $user->alamat,
            'nohp' => $user->nohp,
            'whatsapp' => $user->whatsapp,
            'role' => $user->role->name,
        ];

        return response()->json([
            'detail_user' => $response,
        ], 200);
    }
}
