<?php

namespace App\Http\Controllers\Payment;

use App\Models\XenditInvoice;
use App\Models\Transaksi;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Xendit\Xendit;

class XenditController extends Controller
{
    // private $token = "";
    private $callback_token = "4jNUAnz3yeWbOXPCnqvttFYygisu4ABABDKDxeHqGEFcuhh0";

    public function getVirtualAccount()
    {
        Xendit::setApiKey(env('API_KEY'));

        $getVABanks = \Xendit\VirtualAccounts::getVABanks();

        return view('admin-page.xendit.va', get_defined_vars());
    }

    public function callbackVa(Request $request)
    {
        $external_id = $request->external_id;
        $status = $request->status;
        $order = Order::where('no_va', $external_id)->exists();
        if($order) {
            if($status == 'ACTIVE'){
                $update = Order::where('no_va', $external_id)->update([
                    'status' => 1
                ]);
                if($update > 0){
                    return 'ok';
                }
                return false;
            }
        } else {
            return response()->json([
                'message' => 'Data tidak ada'
            ]);
        }
    }

    public function getTransaksi()
    {
        Xendit::setApiKey(env('API_KEY'));
        $amount_transaksi = 0;

        $params = [
            'types' => 'PAYMENT',
            'query-param'=> 'true'
        ];
        $getBalance = \Xendit\Balance::getBalance('CASH');
        // dd($getBalance);
        $getTransaksi = \Xendit\Transaction::list(array($params));
        // dd($transaksi['data']);
        $transaksi = $getTransaksi['data'];
        foreach($transaksi as $i)
        {
            $amount_transaksi += $i['amount'];
        }
        return view('admin-page.xendit.invoice', get_defined_vars());
    }

    public function getInvoice()
    {
        Xendit::setApiKey(env('API_KEY'));
        $amount_transaksi = 0;

        $params = [
            'types' => 'PAYMENT',
            'query-param'=> 'true'
        ];
        $getBalance = \Xendit\Balance::getBalance('CASH');
        $getTransaksi = \Xendit\Transaction::list(array($params));
        $transaksi = $getTransaksi['data'];
        foreach($transaksi as $i)
        {
            $amount_transaksi += $i['amount'];
        }
        $invoice = XenditInvoice::all();
        $detail_order = Transaksi::with(['order', 'user'])->get();

        return view('admin-page.xendit.invoice', get_defined_vars());
    }

    public function detailInvoice($external_id)
    {
        Xendit::setApiKey(env('API_KEY'));
        $params = [
            'types' => 'PAYMENT',
            'query-param'=> 'true'
        ];
        $getTransaksi = \Xendit\Transaction::list(array($params));
        $transaksi = $getTransaksi['data'];
        $invoice = [];
        foreach($transaksi as $data)
        {
            if(in_array($external_id, array($data['reference_id']))) {
                array_push($invoice, $data);
                return response()->json($data, 200);
            }
        }
    }

}
