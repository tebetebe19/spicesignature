<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\Promo;
use App\Models\About;
use App\Models\Gallery;
use App\Models\Kategori;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VisitorController extends Controller
{
    public function index(Request $request)
    {
        $promo = Promo::all();
        $menu = Menu::with('kategori')->get();

        $about = About::first();
        $data = Gallery::all();
        $kategori = Kategori::orderBy('id', 'ASC')
                    ->with('menu')->get();
        $data_isExist = About::all()->count() > 0;
        $data = Menu::all();

        // $user_isExist = $request->session()->has('users');

        // die($user_isExist);

        return view('visitor-page.landing-page', compact('promo', 'menu', 'about', 'data', 'data_isExist', 'data', 'kategori'));
    }
}
