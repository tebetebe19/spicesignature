<?php

namespace App\Http\Controllers\Admin;

use Auth;
use File;
use Alert;
use DataTables;
use App\Models\Menu;
use App\Models\Kategori;
use Illuminate\Http\Request;
use App\Events\RealTimeMessage;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class MenuController extends Controller
{
    public function __construct()
    {
        $this->path = public_path('assets/img/menu');
        $this->dimensions = ['500'];
    }

    public function index(Request $request)
    {
        // if($request->ajax()) {
        //      $data = Menu::orderBy('id', 'ASC')
        //         ->with(['kategori'])
        //         ->get();
        //     return DataTables::of($data)
        //         ->addIndexColumn()
        //         ->addColumn('action', function($row){
        //             $btn = '<a href="javascript:void(0)" class="edit btn btn-success btn-sm" type="button" data-toggle="modal" data-target="#edit'.$row['id'].'">Edit</a>&nbsp;';
        //             $btn.= '<a href="javascript:void(0)" class="delete btn btn-danger btn-sm" type="button" data-toggle="modal" data-target="#hapus'.$row['id'].'">Delete</a>';
        //             return $btn;
        //         })
        //         ->addColumn('img', function($row){
        //             if($row['img'] == ''){
        //                 $gambar = '<img width="90px" height="90px"  src="'.url('storage/admin/menu/noimage.jpg').'">';
        //             } else {
        //                 $gambar = '<img width="90px" height="90px" class="img-circle"  src="'.url('storage/admin/menu', $row['image']).'">';
        //             }
        //             return $gambar;
        //         })
        //         ->rawColumns(['action', 'img'])
        //         ->make(true);

        // }
        $data = Menu::orderBy('id', 'ASC')->with('kategori')->get();
        $kategori = Kategori::all();
        return view('admin-page.menu.index', get_defined_vars());
    }



    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_menu' => 'required|unique:menu|regex:/^[a-zA-ZÑñ\s]+$/',
            'harga' => 'required'
        ]);

        $data = $request->all();

        $file = $request->file('img');
        $name = time();
        $extension = $file->getClientOriginalExtension();
        $newName = $name . '.' . $extension;
        $path =  $file->move('assets/admin/menu',$newName);

        $store = new Menu;
        $store->nama_menu = $request->nama_menu;
        $store->img = $newName;
        $store->harga = $request->harga;
        $store->kategori_id = $request->kategori_id;
        $store->description = $request->desc;
        $store->save();

        if ($store) {
            event(new RealTimeMessage($request->name . ' baru saja ditambahkan'));
            return redirect()
                ->route('menu.index')
                ->with([
                    alert()->success('Sukses', 'Data berhasil ditambahkan')
                ]);
        } else {
            return redirect()
                ->back()
                ->withInput()
                ->with([
                    alert()->error('Gagal', 'Gagal menambahkan data !')
                ]);
        }
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_menu' => 'required|unique:menu|regex:/^[a-zA-ZÑñ\s]+$/',
            'harga' => 'required'
        ]);

        $cari = Menu::firstWhere('id', $id);

        if($request->img != '' || $request->img != null)
        {
            if(File::exists($this->path .'/'. $cari->img))
            {
                File::delete($this->path.'/'.$cari->img);
                File::delete($this->path.'/500/'.$cari->img);
                $this->validate($request, [
                    'image'=>'image|mimes:jpg,png,jpeg',
                ]);
            }

            //JIKA FOLDERNYA BELUM ADA
            if (!File::isDirectory($this->path)) {
                //MAKA FOLDER TERSEBUT AKAN DIBUAT
                File::makeDirectory($this->path);
            }

            $file = $request->file('img');
            $name = time();
            $extension = $file->getClientOriginalExtension();
            $newName = $name . '.' . $extension;
            $path =  $file->move('assets/admin/menu',$newName);

            $cari->img = $newName;
            $cari->nama_menu = $request->nama_menu;
            $cari->harga = $request->harga;
            $cari->kategori_id = $request->kategori_id;
            $cari->description = $request->desc;
            $cari->save();
            return redirect()->route('menu.index')->with([
                alert()->success('Sukses', 'Data berhasil diubah')
            ]);
        } else
        {
            $cari->nama_menu = $request->nama_menu;
            $cari->harga = $request->harga;
            $cari->kategori_id = $request->kategori_id;
            $cari->description = $request->desc;
            $cari->save();

            return redirect()->route('menu.index')->with([
                alert()->success('Sukses', 'Data berhasil diubah..')
            ]);
        }
    }

    public function destroy($id)
    {
        $menu = Menu::findOrFail($id);
        unlink('assets/admin/menu/'.$menu->img);
        $menu->delete();
        return redirect()->route('menu.index')->with([
            alert()->success('Sukses', 'Data berhasil dihapus..')
        ]);
    }
}
