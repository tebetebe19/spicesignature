<?php

namespace App\Http\Controllers\Admin;

use File;
use Alert;
use App\Models\About;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{
    public function index()
    {
        $about = About::all();
        $data = About::all();
        $data_isExist = About::all()->count() > 0;

        return view('admin-page.about.index', compact('about', 'data_isExist', 'data'));
    }

    public function create()
    {
        return view('admin-page.about.crud');
    }

    public function store(Request $request)
    {
        $data =  $request->all();

        if ($request['id'] == 'null') {
            $query = new About;

        } else {
            $query = About::where('id', $request['id'])->first();
        }

        //IMAGE 1
        $file1 = $request->file('img_1');
        $name1 = $file1->getClientOriginalName();
        $ext1 = $file1->getClientOriginalExtension();
        $img1 = $name1;
        $path1 = $file1->move('assets/admin/about',$img1);

        //IMAGE 2
        $file2 = $request->file('img_2');
        $name2 = $file2->getClientOriginalName();
        $ext2 = $file2->getClientOriginalExtension();
        $img2 = $name2;
        $path2 = $file2->move('assets/admin/about',$img2);

        //IMAGE 3
        $file3 = $request->file('img_3');
        $name3 = $file3->getClientOriginalName();
        $ext3 = $file3->getClientOriginalExtension();
        $img3 = $name3;
        $path3 = $file3->move('assets/admin/about',$img3);

        $query = new About();
        $query->img_1 = $img1;
        $query->img_2 = $img2;
        $query->img_3 = $img3;
        $query->judul = $request['judul'];
        $query->deskripsi = $request['deskripsi'];
        $query->save();

        return redirect()->route('about.index')->with([
            alert()->success('Sukses', 'Data berhasil ditambahkan')
        ]);
    }
    public function update(Request $request, $id)
    {
        $cari = About::firstWhere('id', $id);
        if($request->img_1 != '' || $request->img_1 != null || $request->img_2 != '' || $request->img_2 != null || $request->img_3 != '' || $request->img_3 != null)
        {
            if(File::exists($this->path .'/'. $cari->img))
            {
                File::delete($this->path.'/'.$cari->img);
                File::delete($this->path.'/500/'.$cari->img);
            }

            //JIKA FOLDERNYA BELUM ADA
            if (!File::isDirectory($this->path)) {
                //MAKA FOLDER TERSEBUT AKAN DIBUAT
                File::makeDirectory($this->path);
            }

            //IMAGE 1
            $file1 = $request->file('img_1');
            $name1 = $file1->getClientOriginalName();
            $ext1 = $file1->getClientOriginalExtension();
            $img1 = $name1;
            $path1 = $file1->move('assets/admin/about',$img1);

            //IMAGE 2
            $file2 = $request->file('img_2');
            $name2 = $file2->getClientOriginalName();
            $ext2 = $file2->getClientOriginalExtension();
            $img2 = $name2;
            $path2 = $file2->move('assets/admin/about',$img2);

            //IMAGE 3
            $file3 = $request->file('img_3');
            $name3 = $file3->getClientOriginalName();
            $ext3 = $file3->getClientOriginalExtension();
            $img3 = $name3;
            $path3 = $file3->move('assets/admin/about',$img3);

            // $query = new About();
            $cari->img_1 = $img1;
            $cari->img_2 = $img2;
            $cari->img_3 = $img3;
            $cari->judul = $request['judul'];
            $cari->deskripsi = $request['deskripsi'];
            $cari->save();

            return redirect()->route('about.index');
        } else {
            $cari->judul = $request['judul'];
            $cari->deskripsi = $request['deskripsi'];
            $cari->save();

            return redirect()->route('about.index')->with([
                alert()->success('Sukses', 'Data berhasil diubah')
            ]);
        }
    }
}
