<?php

namespace App\Http\Controllers\Admin;

use Alert;
use Carbon\Carbon;
use App\Models\Menu;
use App\Models\Order;
use App\Models\Riwayat;
use App\Models\Transaksi;
use App\Models\AntrianOrder;
use Illuminate\Http\Request;
use App\Models\DetailTransaksi;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class OrderAdminController extends Controller
{

    public function index()
    {
        $detail_order = Transaksi::with([
            'order' => function($q) {
                return $q->with('xendit_invoice');
            }
            , 'user'])->get();
        $total_transaksi = 0;
        $total_ongkir = 0;
        foreach($detail_order as $i) {
            $total_transaksi += $i->total;
            $total_ongkir += $i->ongkir;
        }
        return view('admin-page.order.index', get_defined_vars());
    }

    public function getDetail(Request $request,$no_resi){
        $data = Transaksi::where('no_resi',$no_resi)->with([
            'detail_transaksi'=> function($query){
                return $query->with([
                    'menu'=> function($q){
                        return $q->with('kategori');
                    }
                ]);
            }
            ,'user', 'shipping'])->first();

        return response()->json($data, 200);
    }

    public function update(Request $request, $id)
    {
        $tgl = Carbon::now()->format('Y-m-d');
        $jam = Carbon::now()->format('H:i:s');
        // $desc = "Pembayaran dari Nomor Resi ". $request->no_resi ." Telah diKonfirmasi oleh Admin";
        $desc2 = "Pesanan dari Nomor Resi ". $request->no_resi ." Sedang diproses pada Kitchen";
        $icon = "fa fa-hamburger";

        $riwayat = new Riwayat();
        $riwayat->no_resi = $request->no_resi;
        $riwayat->tanggal = $tgl;
        $riwayat->jam = $jam;
        // $riwayat->deskripsi = $desc1;
        $riwayat->deskripsi = $desc2;
        $riwayat->icon = $icon;
        $riwayat->save();

        $cari = Order::firstWhere('id', $id);

        $statusTF = "Pembayaran diterima";
        $cari->status_order = $statusTF;
        $cari->save();

        $tgl = $tgl = now()->format('Y-m-d');
        $jml = DB::table('antrian_order')->whereDate('created_at', $tgl)->count();
        $antri = 001+$jml;

        $start = Carbon::now()->format('Y-m-d H:i:s');

        $antrian = new AntrianOrder;
        $antrian->no_antrian = $antri;
        $antrian->transaksi_id = $request->transaksi_id;
        $antrian->no_resi = $request->no_resi;
        $antrian->start = $start;
        $antrian->save();

        return redirect()->route('xendit.invoice')->with([
            alert()->success('Sukses', 'Orderan telah diteruskan ke Kitchen')
        ]);
    }

}
