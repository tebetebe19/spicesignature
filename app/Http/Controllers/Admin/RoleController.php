<?php

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    public function index()
    {
        $data = Role::all();

        return view('admin-page.role.index', compact('data'));
    }

    public function store(Request $request)
    {
        $data = $request->all();

        // dd($data);

        $validated = $request->validate([
            'name' => 'required|unique:role|max:20',
        ]);

        $save = Role::create([
            'name' => $data['name']
        ]);


        return redirect()->route('role.index');
    }

    public function update(Request $request, $id)
    {
        $cari = Role::firstWhere('id', $id);

        $cari->name = $request->name;
        $cari->save();

        return redirect()->route('role.index');
    }

    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role->delete();

        return redirect()->route('role.index');
    }
}
