<?php

namespace App\Http\Controllers\Admin;

use DB;
use Hash;
use Alert;
use DataTables;
use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function tambah(Request $request)
    {
        $data = $request->all();

        $this->validate($request, [
            'name' => 'required|regex:/^[a-zA-ZÑñ\s]+$/',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
        ]);

        $user = new Users();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();

        return redirect()->route('user.index');
    }


    public function index(Request $request)
    {
        if($request->ajax()) {
            $kitchen = User::where('role_id', '2')->with(['role'])->get();
            return DataTables::of($kitchen)
                ->addIndexColumn()
                ->addColumn('tanggal', function($row){
                    $tgl = $row['created_at']->format('l, d-M-Y');
                    return $tgl;
                })
                ->addColumn('action', function($row){
                    $btn = '<a href="javascript:void(0)" title="Edit Data" class="edit btn btn-success btn-sm" type="button" data-toggle="modal" data-target="#kategoriEdit'.$row['id'].'">Edit</a>&nbsp;';
                    $btn.= '<a href="javascript:void(0)" title="Hapus Data" class="delete btn btn-danger btn-sm" type="button" data-toggle="modal" data-target="#kategoriHapus'.$row['id'].'">Hapus</a>';
                    return $btn;
                })
                ->rawColumns(['action', 'tanggal'])
                ->make(true);
        }
        $pengguna = User::where('role_id', '3')->with(['role'])->get();


        return view('admin-page.user.index',get_defined_vars());
    }

    public function create()
    {
        $roles = Role::pluck('name','name')->all();
        return view('users.create',compact('roles'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
        ]);

        $kitchen_user = 2;

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->role_id = $kitchen_user;
        $user->save();

        return redirect()->route('user.index')->with([
            alert()->success('Sukses', 'Data berhasil ditambahkan')
        ]);
    }

    public function show($id)
    {
        $user = User::find($id);
        return view('users.show',compact('user'));
    }

    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::pluck('name','name')->all();
        $userRole = $user->roles->pluck('name','name')->all();

        return view('users.edit',compact('user','roles','userRole'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
            'roles' => 'required'
        ]);

        $input = $request->all();
        if(!empty($input['password'])){
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = Arr::except($input,array('password'));
        }

        $user = User::find($id);
        $user->update($input);
        DB::table('model_has_roles')->where('model_id',$id)->delete();

        $user->assignRole($request->input('roles'));

        return redirect()->route('users.index')
                        ->with([
                            alert()->success('Sukses', 'Data berhasil diubah..')
                        ]);
    }

    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('users.index')
                        ->with([
                            alert()->success('Sukses', 'Data berhasil dihapus !')
                        ]);
    }
}
