<?php

namespace App\Http\Controllers\Admin;

use File;
use App\Models\Promo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PromoController extends Controller
{
    public function __construct()
    {
        $this->path = public_path('assets/img/promo');
        $this->dimensions = ['500'];
    }

    public function index()
    {
        $data = Promo::all();
        return view('admin-page.promo.index', get_defined_vars());
    }

    public function store(Request $request)
    {
        $data =  $request->all();

        $file = $request->file('img');
        $name = time();
        $extension = $file->getClientOriginalExtension();
        $newName = $name . '.' . $extension;
        $path =  $file->move('assets/admin/promo',$newName);

        $store = Promo::create([
            'nama_promo' => $data['nama_promo'],
            'img' => $newName,
            'deskripsi' => $data['deskripsi'],
        ]);

        return redirect()->route('promo.index');
    }

    public function update(Request $request, $id)
    {
        $cari = Promo::firstWhere('id', $id);

        if($request->img != '' || $request->img != null)
        {
            if(File::exists($this->path .'/'. $cari->img)){
                File::delete($this->path.'/'.$cari->img);
                File::delete($this->path.'/500/'.$cari->img);
                $this->validate($request, [
                'image'=>'image|mimes:jpg,png,jpeg',
            ]);
            }

            //JIKA FOLDERNYA BELUM ADA
            if (!File::isDirectory($this->path)) {
                //MAKA FOLDER TERSEBUT AKAN DIBUAT
                File::makeDirectory($this->path);
            }

            $file = $request->file('img');
            $name = time();
            $extension = $file->getClientOriginalExtension();
            $newName = $name . '.' . $extension;
            $path =  $file->move('assets/admin/promo',$newName);

            $cari->img = $newName;
            $cari->nama_promo = $request->nama_promo;
            $cari->deskripsi = $request->deskripsi;
            $cari->save();

            return redirect()->route('promo.index');
        } else
        {
            $cari->nama_promo = $request->nama_promo;
            $cari->deskripsi = $request->deskripsi;
            $cari->save();

            return redirect()->route('promo.index');
        }
    }

    public function destroy($id)
    {
        $promo = Promo::findOrFail($id);
        unlink('assets/admin/promo/'.$promo->img);
        $promo->delete();
        return redirect()->route('promo.index')->with('success', 'Data Berhasil diHapus!');
    }
}
