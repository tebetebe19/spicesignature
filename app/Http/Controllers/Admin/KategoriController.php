<?php

namespace App\Http\Controllers\Admin;

use Alert;
use DataTables;
use App\Models\Kategori;
use App\Models\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class KategoriController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax()) {
            $data = Kategori::orderBy('id', 'ASC')->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = '<a href="javascript:void(0)" title="Edit Data" class="edit btn btn-success btn-sm" type="button" data-toggle="modal" data-target="#kategoriEdit'.$row['id'].'">Edit</a>&nbsp;';
                    $btn.= '<a href="javascript:void(0)" title="Hapus Data" class="delete btn btn-danger btn-sm" type="button" data-toggle="modal" data-target="#kategoriHapus'.$row['id'].'">Hapus</a>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);

        }
        $data = Kategori::all();
        // dd($data);

        return view('admin-page.kategori.index', get_defined_vars());
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $validated = $request->validate([
                'kategori' => 'required|unique:kategori|max:20',
            ]);

            $save = Kategori::create([
                'kategori' => $data['kategori']
            ]);

        return redirect()->route('kategori.index')->with([
            alert()->success('Sukses', 'Data berhasil ditambahkan')
        ]);
    }

    public function update(Request $request, $id)
    {
        $cari = Kategori::firstWhere('id', $id);

        $cari->kategori = $request->kategori;
        $cari->save();

        return redirect()->route('kategori.index')->with([
            alert()->success('Sukses', 'Data berhasil diubah')
        ]);
    }

    public function destroy($id)
    {
        $kategori = Kategori::findOrFail($id);
        $kategori->delete();

        return redirect()->route('kategori.index')->with([
            alert()->success('Sukses', 'Data berhasil dihapus')
        ]);;
    }
}
