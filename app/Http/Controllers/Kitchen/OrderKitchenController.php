<?php

namespace App\Http\Controllers\Kitchen;

use App\Models\Kurir;
use App\Models\AntrianOrder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderKitchenController extends Controller
{

    public function index()
    {
        $antrian = AntrianOrder::with([
            'transaksi' => function($query) {
                return $query->with([
                    'user', 'order', 'detail_transaksi' => function($q) {
                        return $q->with([
                            'menu' => function($qry) {
                                return $qry->with(['kategori']);
                            }
                        ]);
                    }
                ]);
            }
        ])->get();

        $kurir = Kurir::with(['antrian_order',
                'transaksi' => function ($query) {
                    return $query->with(['user', 'detail_transaksi' => function($qry){
                        return $qry->with([
                            'menu' => function($q) {
                                return $q->with(['kategori']);
                            }
                        ]);
                    }]);
            }
        ])->get();

        return view('kitchen-page.order.index', get_defined_vars());
    }
}
