<?php

namespace App\Http\Controllers\Kitchen;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class DashboardKitchenController extends Controller
{
    public function index(Request $request)
    {
        $jualByDay = DB::table('detail_transaksi')
                ->select('menu.nama_menu', DB::raw('sum(detail_transaksi.qty) as qty'))
                ->join('menu', 'menu.id', '=', 'detail_transaksi.menu_id')
                ->groupBy('menu.nama_menu')
                ->whereDate('detail_transaksi.created_at', Carbon::today())
                ->orderBy('qty', 'DESC')
                ->get();

        if ($request->ajax()) {
            $events = Order::all();

            return response()->json($events);
            // dd($events);
        }
        return view('kitchen-page.dashboard.index', compact('jualByDay'));
    }
}
