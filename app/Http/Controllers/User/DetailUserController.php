<?php

namespace App\Http\Controllers\User;

use App\Models\DetailUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DetailUserController extends Controller
{
    public function index()
    {
        $data_isExist = DetailUser::join('users', 'users.id', '=', 'detail_users.user_id')
                        ->where('detail_users.user_id', Auth::id())
                        ->count() > 0;

        $data = DetailUser::join('users', 'users.id', '=', 'detail_users.user_id')
                ->where('detail_users.user_id', Auth::id())
                ->first();

        return view('user-page.detail-user.index', compact('data_isExist', 'data'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'alamat' => 'required',
            'nohp' => 'required',
            'whatsapp' => 'required'
        ]);

        $detailUser = new DetailUser;
        $detailUser->user_id = Auth::id();
        $detailUser->alamat = $request->alamat;
        $detailUser->nohp = $request->nohp;
        $detailUser->whatsapp = $request->whatsapp;
        $detailUser->save();

        if ($detailUser) {
            return redirect('user/detail-user')
                ->with([
                    'success' => 'Sukses', 'Data Anda berhasil diUpdate !'
                ]);
        } else {
            return redirect()
                ->back()
                ->withInput()
                ->with([
                    'error' => 'Gagal... Try Again!!'
                ]);
        }
    }
}
