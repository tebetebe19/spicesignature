<?php

namespace App\Http\Controllers\User;

use Xendit\Xendit;
use App\Models\Menu;
use App\Models\User;
use App\Models\Kategori;
use App\Models\DetailUser;
use GuzzleHttp\Client;
use App\Models\ShippingAddress;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class CartController extends Controller
{
    private $token = "xnd_development_qRbj2yfZ7R0M7d138S5RMfk20Jj0wKsVi0EGDqdCqN0NSUYXrevLxFhKH2Kx";

    public function index()
    {
        $check_alamat = ShippingAddress::where('user_id', Auth::id())->count() > 0;

        if($check_alamat) {
            $shipping = ShippingAddress::where('user_id', Auth::id())->first();
            $lat1 = -6.24279334337993;
            $long1 = 106.78335050175951;
            $lat2 = $shipping->lat;
            $long2 = $shipping->long;
            $key = "pk.eyJ1Ijoic3BpY2VzaWduYXR1cmUiLCJhIjoiY2wzdG1hanYwMjFtcjNicGU2YWRkeWc0cCJ9.vbGzNcWEjq__vqVg8EgAUA";

            $client = new Client();
            $url = "https://api.mapbox.com/directions/v5/mapbox/cycling/" .$long1."%2C".$lat1."%3B".$long2."%2C".$lat2."?alternatives=true&geometries=geojson&language=en&overview=simplified&steps=true&access_token=".$key;
            $response = $client->request('GET', $url, [
                'verify' => false,
            ]);

            $responseBody = json_decode($response->getBody());
            $distance = json_encode($responseBody->routes[0]->distance);
            $duration = json_encode($responseBody->routes[0]->duration);
            // die($distance/1000);

            $tanggal = now()->format('dmy');
            $pass = substr(str_shuffle("0123456789ABCDEFGHIKLMNOPQRSTUVWXYZ"), 0, 3);
            $no_resi = $tanggal.$pass;
            $menu = Menu::orderBy('id', 'ASC')
                    ->with(['kategori'])
                    ->get();

            $kategori = Kategori::orderBy('id', 'ASC')
                        ->with('menu')->get();

            $cartItems = \Cart::getContent();
            $menu_isExist = Menu::all()->count() > 0;
            $check = User::orderBy('id', 'ASC')
                        ->where('users.id', Auth::id())
                        ->first();
        } else {
            session()->flash('error', 'Sebelum melakukan pemesanan, Silahkan diisi terlebih dahulu alamat pengiriman anda !');
            return redirect()->route('shipping-address.index');
        }

        return view('user-page.cart.index', get_defined_vars());
    }

    public function cartList()
    {
        $cartItems = \Cart::getContent();

        return view('cart', compact('cartItems'));
    }

    public function addToCart(Request $request)
    {
        \Cart::add([
            'id' => $request->id,
            'name' => $request->name,
            'price' => $request->price,
            'quantity' => $request->quantity,
            'attributes' => array(
                'image' => $request->image,
            )
        ]);
        session()->flash('success', 'Menu berhasil ditambahkan ke keranjang');

        return redirect()->route('cart.index');
    }

    public function updateCart(Request $request)
    {
        \Cart::update(
            $request->id,
            [
                'quantity' => [
                    'relative' => false,
                    'value' => $request->quantity
                ],
            ]
        );
        session()->flash('success', 'Keranjang telah diUpdate');
        return redirect()->route('cart.list');
    }

    public function removeCart(Request $request)
    {
        $hapus = \Cart::remove($request->id);
        // die($hapus);
        session()->flash('success', 'Item berhasil di Hapus');
        return redirect()->route('cart.index');
    }

    public function clearAllCart()
    {
        \Cart::clear();

        session()->flash('success', 'Semua item di keranjang telah di Hapus');
        return redirect()->route('cart.index');
    }
}
