<?php

namespace App\Http\Controllers\User;

use Mail;
use Xendit\Xendit;
use Carbon\Carbon;
use App\Models\Order;
Use App\Mail\OrderMail;
use App\Models\Riwayat;
use App\Models\Transaksi;
use App\Models\XenditInvoice;
use App\Models\ShippingAddress;
use Illuminate\Http\Request;
use App\Models\DetailTransaksi;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function index()
    {
        $detail_order = Transaksi::where('user_id', Auth::id())->with(['order', 'user'])->get();

        return view('user-page.order.index', compact('detail_order'));
    }

    public function getDetail(Request $request,$no_resi){
        $data = Transaksi::where('no_resi',$no_resi)->with([
            'detail_transaksi'=> function($query){
                return $query->with([
                    'menu'=> function($q){
                        return $q->with('kategori');
                    }
                ]);
            }
            ,'user', 'shipping'])->first();

        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        Xendit::setApiKey(env('API_KEY'));
        $tanggal = now()->format('dmy');
        $pass = substr(str_shuffle("0123456789"), 0, 5);
        $uniqueID = $tanggal.$pass;
        $invoice = [
            "external_id" => "SS-" .$uniqueID,
            "description" => "Pembayaran dengan No. Resi ". $request->no_resi,
            "amount" => $request->grand_total,
            "customer" => [
                'name' => $request->nama,
                'email' => $request->email,
                'mobile_number' => $request->nohp,
            ],
            "customer_notification_preference" => [
                'invoice_created' => [
                    'whatsapp',
                    'sms',
                    'email',
                    'viber'
                ],
                'invoice_reminder' => [
                    'whatsapp',
                    'sms',
                    'email',
                    'viber'
                ],
                'invoice_paid' => [
                    'whatsapp',
                    'sms',
                    'email',
                    'viber'
                ],
                'invoice_expired' => [
                    'whatsapp',
                    'sms',
                    'email',
                    'viber'
                ]
            ]
        ];
        $createInvoice = \Xendit\Invoice::create($invoice);
        $id = $createInvoice['id'];
        $getInvoice = \Xendit\Invoice::retrieve($id);
        // print_r($getInvoice); die();
        // dd($createInvoice);
        // die($createInvoice['customer']['mobile_number']);

        $status_awal = "Menunggu Pembayaran";

        //INSERT DETAIL TRANSAKSI
        foreach($request->menu_id as $key => $menu_id){
            $detail = new DetailTransaksi();

            $detail->no_resi = $request->no_resi;
            $detail->menu_id = $menu_id;
            $detail->qty = $request->qty[$key];
            $detail->subtotal = $request->subtotal[$key];
            $detail->save();
        }
        //INSERT XENDIT INVOICE
        $invoice = new XenditInvoice;
        $invoice->id = $createInvoice['id'];
        $invoice->external_id = $createInvoice['external_id'];
        $invoice->user_id = $createInvoice['user_id'];
        $invoice->no_resi = $request->no_resi;
        $invoice->status = $createInvoice['status'];
        $invoice->merchant_name = $createInvoice['merchant_name'];
        $invoice->amount = $createInvoice['amount'];
        $invoice->description = $createInvoice['description'];
        $invoice->expiry_date = $createInvoice['expiry_date'];
        $invoice->invoice_url = $createInvoice['invoice_url'];
        $invoice->customer_email = $createInvoice['customer']['email'];
        $invoice->customer_mobile_number = $createInvoice['customer']['mobile_number'];
        $invoice->save();
        //INSERT TRANSAKSI
        $transaksi = new Transaksi();
        $transaksi->no_resi = $request->no_resi;
        $transaksi->user_id = $request->user_id;
        $transaksi->shipping_id = $request->shipping_id;
        $transaksi->jarak = $request->jarak;
        $transaksi->ongkir = $request->ongkir;
        $transaksi->total = $request->total;
        $transaksi->grand_total = $request->grand_total;
        $transaksi->save();

        //INSERT ORDER
        $order = new Order();
        $order->no_resi = $request->no_resi;
        $order->status_order = $status_awal;
        $order->save();

        $tgl = Carbon::now()->format('Y-m-d');
        $jam = Carbon::now()->format('H:i:s');
        $desc = "Order menu dengan Nomor Resi ". $request->no_resi;
        $icon = "fa fa-shopping-cart";
        //INSERT RIWAYAT ORDER
        $riwayat = new Riwayat();
        $riwayat->no_resi = $request->no_resi;
        $riwayat->tanggal = $tgl;
        $riwayat->jam = $jam;
        $riwayat->deskripsi = $desc;
        $riwayat->icon = $icon;
        $riwayat->save();


        Mail::to($request->email)->send(new OrderMail());

        if(Mail::failures()){
            return response()->with([
                alert()->warning('Gagal', 'Pesanan Gagal')
            ]);
        } else {
            \Cart::clear();
            session()->flash('newurl', $createInvoice['invoice_url']);
            return redirect()->back()->with([
                alert()->success('Sukses', 'Link Pembayaran telah dikirim ke Email dan Whatsapp Anda')
            ]);
            // return redirect($createInvoice['invoice_url']);
        }
    }

    public function update(Request $request, $id)
    {
        $tgl = Carbon::now()->format('Y-m-d');
        $jam = Carbon::now()->format('H:i:s');
        $desc = "Nomor Resi ". $request->no_resi ." Telah upload bukti transfer";
        $icon = "fa fa-upload";

        $riwayat = new Riwayat();
        $riwayat->no_resi = $request->no_resi;
        $riwayat->tanggal = $tgl;
        $riwayat->jam = $jam;
        $riwayat->deskripsi = $desc;
        $riwayat->icon = $icon;
        $riwayat->save();

        $cari = Order::firstWhere('id', $id);

        $file = $request->file('bukti_transfer');
        $name = time();
        $extension = $file->getClientOriginalExtension();
        $newName = $name . '.' . $extension;
        $path =  $file->move('assets/admin/bukti_transfer',$newName);
        $statusTF = "Pembayaran sedang direview";
        $cari->bukti_transfer = $newName;
        $cari->status_order = $statusTF;

        $cari->save();
        return redirect()->route('order.index');
    }
}
