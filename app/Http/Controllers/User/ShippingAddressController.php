<?php

namespace App\Http\Controllers\User;

use App\Models\District;
use App\Models\Village;
use App\Models\Regency;
use App\Models\ShippingAddress;
use App\Models\Province;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class ShippingAddressController extends Controller
{
    public function index()
    {
        $prov = Province::all();
        $check = ShippingAddress::where('user_id', Auth::id())->count() == 1;
        $data = ShippingAddress::where('user_id', Auth::id())->with([
            'villages' => function($query) {
                return $query->with([
                    'district' => function ($q) {
                        return $q->with([
                            'regency' => function ($qry) {
                                return $qry->with('province');
                            }
                        ]);
                    }
                ]);
            }
            ])->get();
        // dd($data);
        return view('user-page.shipping.index', compact('prov', 'data', 'check'));
    }

    public function getKota($id)
    {
        $kota = Regency::where('province_id', $id)->pluck("name", "id");
        // die($kota);
        return json_encode($kota);
    }

    public function getKecamatan($id)
    {
        $kecamatan = District::where('regency_id', $id)->pluck("name", "id");
        // die($kota);
        return json_encode($kecamatan);
    }

    public function getKelurahan($id)
    {
        $kelurahan = Village::where('district_id', $id)->pluck("name", "id");
        // die($kota);
        return json_encode($kelurahan);
    }

    public function store(Request $request)
    {
        $data = new ShippingAddress;
        $data->user_id = $request->user_id;
        $data->villages_id = $request->kelurahan;
        $data->detail_alamat = $request->alamat;
        $data->lat = $request->lat;
        $data->long = $request->long;
        $data->save();

        return redirect()->route('shipping-address.index');
    }
}
