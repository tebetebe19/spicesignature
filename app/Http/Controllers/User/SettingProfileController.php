<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class SettingProfileController extends Controller
{

    public function index()
    {
        $check = User::orderBy('id', 'ASC')
                    ->where('users.id', Auth::id())
                    ->first();
        $data = User::orderBy('id', 'ASC')
                    ->where('users.id', Auth::id())
                    ->first();
        return view('user-page.setting-profile.index', compact('check', 'data'));
    }

    public function update(Request $request, $id)
    {
        $cari = User::firstWhere('id', $id);

        $cari->alamat = $request->alamat;
        $cari->nohp = $request->nohp;
        $cari->whatsapp = $request->whatsapp;

        $cari->save();

        return redirect()->route('setting-profile.index');
    }
}
