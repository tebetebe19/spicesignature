<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Transaksi;
use App\Models\Riwayat;
use Illuminate\Support\Facades\Auth;

class DashboardUserController extends Controller
{
    public function index()
    {
        $transaksi = Transaksi::where('user_id', Auth::id())
                ->with([
                    'order', 'user', 'riwayat'
                    ])->get();
        return view('user-page.dashboard.index', compact('transaksi'));
    }

    public function getRiwayat(Request $request,$no_resi)
    {
        $data = Transaksi::where('no_resi', $no_resi)
            ->with(['riwayat'])
            ->first();
        return response()->json($data, 200);
    }
}
